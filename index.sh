#!/bin/sh

help(){
cat << EOF
Usage:
  dev.sh <command> [options]

Commands:
  build         Build site
  dev           Create a test build
  tag           Generate a new git tag
  delete        Delete a git tag
  submit        Submit my site to search engines

Options:
  -h, --help    Display the help message
EOF
return 0
}
build() {
  hugo --minify
  return $?
}
deploy(){
  git config credential.helper cache
  git pull --tags --quiet --rebase
  git tag -l | more
  printf '\n\n%s' 'Type the tag name you want to create: '
  read -r TAG
  git tag "$TAG"
  git push --tags
  git push --quiet
  return 0
}
delete(){
  git tag -l | more
  printf '\n\n%s' 'Type the tag name you want to delete: '
  read -r TAG
  git tag -d "$TAG"
  git push origin --delete "$TAG"
  return 0
}
submit() {
  SITEMAP_URL="https://cizordj.codeberg.page/sitemap.xml"
  GOOGLE_URL="https://www.google.com/webmasters/sitemaps/ping"
  BING_URL="https://www.bing.com/ping"
  curl "$BING_URL?sitemap=$SITEMAP_URL"
  curl "$GOOGLE_URL?sitemap=$SITEMAP_URL"
}
dev() {
  rm -rf public/*
  hugo --minify --baseURL 'http://localhost:8080'
}

case "$1" in
  -h|--help)
    help;;
  deploy)
    deploy;;
  build)
    build;;
  delete)
    delete;;
  submit)
    submit;;
  dev)
    dev;;
  *)
    help;;
esac
exit $?
