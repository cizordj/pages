+++
title = "Better camera on oreo ROMs"
date = "2019-03-10"
author = "Cézar Campos"
description = "The biggest villain on every Samsung phone is that denoise thingy, in this article I show you how to get rid of it."
tags = ["Aleatórios"]
language = "en-us"
+++

    Originally published on Xda-developers' forum

# Introduction

If you don't like the camera's quality it is because the google camera app is
not the best for the grand prime, actually this is the problem of all ROMs based
on the pure android, isn't the image clear enough? does the image looks blurred?

I've been comparing my camera on android oreo with my another phone (both are
the same model), one has the stock Android and the other one has the pure
Android 8 and the differences are huge, how to work around this problem?

Well... I have researched and talked to devs about this and it turns out that
the drivers of the camera are the same as the stock firmware, but the only thing
what changes the quality is how the image is processed by the camera
application.

So who's the one to blame? The camera app.

What is the solution?

The real solution is to use ROMs based on the stock ROM, i.e. touchwiz, but I
have noticed that many people still prefer using the pure Android so this
solution is not possible. There is still another way.

# CyanogenMod camera and fine adjustments

That's what you read, with the 'old' Cyanogen camera app you can have better
pictures. First, install this app:

[https://play.google.com/store/apps/details?id=com.maartendekkers.cyanapps](https://play.google.com/store/apps/details?id=com.maartendekkers.cyanapps)

It's called Cyan Apps if the link isn't working and then...

# Choose 'camera' on CM's 13 section

Note: Don't choose the CM 14.1 version because it crashes when you start
recording a video or change to frontal camera.

Download and install it.

# Adjustments

Open the camera and go to settings, drag down until the last options and turn
off the Denoise option, then go to the sharpness and set it to the level 0 (or
do whatever you think is better) and go to the picture quality and set it to
100%. The rest of the options you can leave in the default level.

# Optional

You can alternatively with root access make this a system application and delete
the other one just to have only one default for your ROM

In the thumbnails I want you to pay attention in the sharpness of the image and
the quality, I hope you don't mind if I'm a bad photographer.

UPDATE: I know the link is broken, it's because Cyan Apps has been removed from
playstore, I don't know why, but you know now the secret to make the image
good-looking, you have to turn off denoise option which comes by default turned
on in all Samsung cameras. Find an APP that can make this for you. Best Regards.

![](/artigos/better-camera-on-oreo-roms/figura1.jpg)
![](/artigos/better-camera-on-oreo-roms/figura2.jpg)
![](/artigos/better-camera-on-oreo-roms/figura3.jpg)
