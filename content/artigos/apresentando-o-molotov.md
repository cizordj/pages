+++
title = "Apresentando o Molotov"
date = "2021-01-17"
author = "Cézar Campos"
description = "O Molotov é um programa que eu criei afim de poder criar pen drives bootáveis de Windows 10 no Linux. Aqui eu apresento ele para a comunidade."
tags = ["Artigo vivaolinux"]
+++

# O que é Molotov

Olá, pessoal.

Eu gostaria de apresentar para vocês a solução que trago para as pessoas que
querem criar um pendrive bootável do Windows 10, através do Linux usando um
método bem simples, o Molotov.

O Molotov é um utilitário de linha de comando que criei após o bug das
dependências do [WoeUSB](https://github.com/slacka/WoeUSB/issues/311). A razão
pela qual eu criei este programa é porque o WoeUSB possui alguns problemas que
eu viso corrigir com a criação do Molotov.

Quais problemas seriam esses ?

- Código muito grande
- Muitas dependências
- Alto acoplamento
- Difícil de manter
- Não segue a filosofia Unix

Além disso, ao tentar ler o código - fonte, encontrei vários comentários de
pessoas que não entendiam o que aquela parte do código faz e por essa razão
ninguém tinha coragem de tirar fora, entre outras coisinhas mais. O meu objetivo
aqui não é difamar ninguém e muito menos o criador do WoeUSB pelos problemas que
apresentei acima e sim explicar o por que meu programa é melhor.

O Molotov é:

- Fácil de usar
- Fácil de ler
- Pequeno
- Usa dependências nativas do Linux

Para usar o Molotov é bem simples, vá para a minha página no GitHub e baixe o
pacote ".deb":

[Releases · cizordj / molotov · GitHub](https://github.com/cizordj/molotov/releases/)

{{< figure src="/artigos/apresentando-o-molotov/figura1.png" alt="Captura de tela mostrando os arquivos da primeira soltura do programa molotov ainda em fase beta. Fonte: O autor (2021)" position="center" caption="Figura 1 - Primeira liberação do Molotov" captionPosition="center" >}}

Após baixar o pacote, você instala ele com o seguinte comando:

```console
# apt-get install ./molotov_1.0_all.deb
```

E agora para usá-lo, é só digitar isso no terminal:

```console
# molotov -i <imagem-iso> -d <seu_pendrive> [opções]
```

Simples, não? Lembrando que o `<seu_pendrive>` dever ser um dispositivo bloco
tal como `/dev/sdX`.

{{< figure src="/artigos/apresentando-o-molotov/figura2.png" alt="Captura de tela mostrando o texto de ajuda do molotov em um terminal Linux. Fonte: O autor (2021)" position="center" caption="Figura 2 - Molotov em execução" captionPosition="center" >}}

Além disso, o Molotov segue os padrões de codificação
[GNU](https://www.gnu.org/prep/standards/standards.html#Command_002dLine-Interfaces)
para linha de comando, possui um manual de instruções e é nativo para o Debian
Bullseye.

Então, espero que daqui a alguns anos o Molotov esteja em todas as distribuições
derivadas.

[Um abraço a todos.](/molotov)
