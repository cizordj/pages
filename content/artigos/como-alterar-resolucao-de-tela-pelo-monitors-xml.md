---
title: "Como alterar resolução de tela pelo monitors.xml"
date: "2020-07-16"
author: "Cézar Campos"
description: "Nesse artigo eu ensino como se faz para mudar a resolução de tela no Linux através de um arquivo de configuração chamado monitors.xml"
tags: ["Dica vivaolinux"]
language: "pt-br"
---

As interfaces modernas não respeitam mais o _Xorg.conf_, isto é,
se você fixar uma resolução de tela no seu arquivo _Xorg_ as
interfaces ainda podem ignorá-lo e exibir a imagem na resolução de tela
errada. Essa dica visa mostrar como fixar a resolução de tela em
interfaces modernas como _Gnome_ e o _Pantheon_ sem que o
seu monitor seja reconhecido.

As pessoas que usam um adaptador no cabo de vídeo (como eu) sofrem do
mal do monitor não ser reconhecido e isso acarreta na tela com a
resolução errada. Em qualquer distribuição Linux e em qualquer interface
gráfica a imagem sempre vai estar errada e as pessoas têm que recorrer a
métodos manuais para fixar a resolução de tela.

O Gnome e o Pantheon lêem um arquivo chamado monitors.xml para definir
uma resolução de tela, esse arquivo encontra-se em dois lugares:

Na pasta HOME do usuário: `/home/$USER/.config/monitors.xml`
e na pasta da tela de login do sistema:
`/var/lib/gdm3/.config/monitors.xml`

Dependendo de qual seja a sua interface a pasta de login pode variar, se
você tem um monitor que não é reconhecido então você terá que preencher
esses dois arquivos a fim de resolver o problema. O formato desse
arquivo é mais ou menos esse:

```xml
<monitors version="2">
  <configuration>
    <logicalmonitor>
      <x>0</x>
      <y>0</y>
      <scale>1</scale>
      <primary>yes</primary>
      <monitor>
        <monitorspec>
          <connector>DVI-I-0</connector>
          <vendor>unknown</vendor>
          <product>unknown</product>
          <serial>unknown</serial>
        </monitorspec>
        <mode>
          <width>1280</width>
          <height>720</height>
          <rate>59.999485015869141</rate>
        </mode>
      </monitor>
    </logicalmonitor>
  </configuration>
</monitors>
```

Mas calma, você não precisa ser um expert e escrever isso do zero, vamos
com um passo a passo mais simples.

No Elementary OS, vá em:
Configurações do sistema → Telas → Configurações do monitor
defina a resolução correta do seu monitor de salve.
{{< figure src="/artigos/como-alterar-resolucao-de-tela-pelo-monitors-xml/figura1.png" alt="Captura de tela do Elementary OS mostrando as configurações de resolução de tela. Fonte: O autor (2020)" position="center" caption="Figura 1" captionPosition="center" >}}

Agora você terá o arquivo monitors.xml na sua pasta de usuário, para
aplicar a mesma resolução na tela de login copie o arquivo para o
diretório do sistema.

No caso do Elementary OS:
```console
$ sudo cp /home/$USER/.config/monitors.xml /var/lib/lightdm/.config/
```
Se você estiver usando a interface Gnome, o comando é esse:
```console
$ sudo cp /home/$USER/.config/monitors.xml /var/lib/gdm3/.config/
```

Pronto! A resolução de tela agora estará fixada tanto na tela de login
quanto na sessão do usuário.

É isso.

* Testado no Debian Gnome e Elementary OS.
