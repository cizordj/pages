+++
title = "Como desbloquear o bootloader em celulares Samsung"
date = "2020-04-27"
author = "Cézar Campos"
description = "Como desbloquear os bootloaders dos celulares da Samsung, este é o primeiro passo para você que quer customizar a sua ROM do Android. Divirta-se."
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado em
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/Como-Desbloquear-o-Bootloader-em-Celulares-Samsung/)

Olá. Se você pretende fazer _root_ no seu smartphone _Samsung_, então o primeiro
obstáculo a se passar é o bloqueio do _bootloader_. Infelizmente, nem todos os
aparelhos podem ser desbloqueados, pois isso depende do país, do modelo do
smartphone e da política da Samsung para aquele país.

**NOTA**: vale ressaltar que o desbloqueio do bootloader causa perda de garantia
e o autor não se responsabiliza por isso, os seus dados também serão excluídos,
então faça algum becape.

# O que é bootloader

O bootloader, também chamado de gerenciador de inicialização, é o programa
responsável por inicializar o sistema Android, a recovery e o modo download. Em
smartphones Samsung, o bootloader possui um mecanismo que impede você de
instalar versões customizadas do Android, sejam elas da própria Samsung ou não,
e além do mais o bootloader somente pode ser atualizado para versões mais novas,
nunca para as mais antigas.

Configurações → Sobre o telefone → Informações de software → Versão da banda de
base

```
A205GNDXS5ATA2
```

Preste atenção no quinto último dígito deste código, esta é a versão do seu
bootloader, no meu caso a versão é **5**.

# Habilitando o modo desenvolvedor

Habilite o modo desenvolvedor, vá em: Configurações → Sobre o telefone →
Informações do software - Toque o "Número de compilação" 7 vezes.

Isso vai mostrar as opções do desenvolvedor nas configurações do Android. Vá
para as opções do desenvolvedor: Configurações → Opções do desenvolvedor.

{{< figure src="/artigos/como-desbloquear-o-bootloader-em-celulares-samsung/figura1.jpg" alt="Captura de tela nas opções do desenvolvedor. Fonte: O autor (2020)" caption="Figura 1 - Captura de tela nas opções do desenvolvedor" position="center" captionPosition="center" >}}

Como você pode ver, a opção "desbloquear bootloader" não está presente, então
você vai ter que esperar 7 dias para isso (teoricamente). Esse é um truque que a
Samsung implementou para impedir que os desenvolvedores desbloqueiem o
bootloader por engano.

Existe uma forma de contornar essa espera? Sim:

- Primeiro, você deve desligar as conexões com a internet, deixe seusmartphone
  totalmente offline.
- Ajuste a data manualmente para 8 dias antes de hoje.
- No momento que estou escrevendo isso (dia 22 de abril), eu ajustei a data para
  14 de abril.
- Agora conecte o aparelho à Internet.
- Vá em Configurações e atualize o Android: Configurações → Atualização de
  software → Baixar e instalar.

Você vai ver uma mensagem de toast dizendo "Registrando o dispositivo", este
truque fará com que o smartphone 'pense' que foi desbloqueado 8 dias atrás e vai
liberar a opção no menu dos desenvolvedores para você desbloquear o bootloader.

Agora, você pode voltar à data atual ajustando a data e hora conforme a rede.

Vá nas opções do desenvolvedor:

{{< figure src="/artigos/como-desbloquear-o-bootloader-em-celulares-samsung/figura2.jpg" alt="Captura de tela nas opções do desenvolvedor. Fonte: O autor (2020)" caption="Figura 2 - Captura de tela nas opções do desenvolvedor" position="center" captionPosition="center" >}}

...e selecione a opção **"Desbloqueio por OEM"**.

No meu está em cinza, porque eu já fiz este processo antes, mas para você não
acabou ainda. O próximo passo é reiniciar o celular no modo download, faça isso
via ADB ou teclas de combinação:

```console
$ adb reboot bootloader
```

# Via teclas de combinação

Desligue o celular, conecte-o no PC e antes que ele acenda a tela, pressione os
dois botões de volume ao mesmo tempo - Testado no _Galaxy-A20_.

Uma vez em modo download, segure o botão de volume pra cima até ele desbloquear
o bootloader.

NOTA: este procedimento vai apagar todos os seus dados e reiniciar
automaticamente.

Agora o bootloader foi liberado? A resposta ainda é não, temos que confirmar o
desbloqueio para que o aparelho não rejeite os binários modificados futuramente
evitando esse famigerado erro:

    Only official released binaries are allowed to be flashed

Veja:
[Only Official binaries are allowed to be flashed](https://forum.xda-developers.com/galaxy-note-8/help/official-released-binaries-allowed-to-t3681883)

Passe as configurações iniciais do aparelho, de preferência conectado à
Internet. Não coloque muitos dados aqui, pois o dispositivo será formatado
depois quando você fizer root. Ative as opções de desenvolvedor e confirme se a
opção "Desbloqueio por OEM" está acinzentada, assim como a imagem acima.

Pronto! Seu bootloader deve aceitar agora binários não oficiais, permitindo você
a fazer root e instalar custom ROMs.

Over 'n out!
