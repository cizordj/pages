+++
title = "Sites que eu acho interessantes"
date = "2021-11-26"
author = "Cézar Campos"
description = "Essa é uma lista de sites que eu encontrei na internet e considero interessantes."
tags = ["Utilidades"]
+++

Essa é uma lista de sites que eu encontrei por aí na internet e que considero
interessantes por alguma razão, quase sempre esses são sites pequenos e
estáticos que contém conteúdo útil para desenvolvimento pessoal ou que contém
alguma crítica social. Esses tipos de sites dificilmente aparecem no Google e eu
dificilmente salvo os links deles nos meus favoritos, então eu estou expondo
eles aqui publicamente para que qualquer pessoa além de mim possa desfrutar
deles.

0. [Carolina Knoll](https://carolinaknoll.github.io/ 'Site que eu considero o visual bonito')
1. [Cliceu Laibida](https://www.recantodasletras.com.br/autor_textos.php?id=197096 'Blogue do meu antigo professor de português.')
2. [Don't ask to ask, just ask](https://dontasktoask.com/ 'Interessante para quem faz perguntas bobas nos fóruns')
3. [Easy peasy method](https://easypeasymethod.org/ 'Site que ensina como lidar com a masturbação em excesso')
4. [FF Profiler](https://ffprofile.com/ 'O primeiro site que você deve visitar após instalar o Firefox')
5. [How I experience today](https://how-i-experience-web-today.com/ 'Um site que mostra um retrato de como está a internet hoje em dia')
6. [HTML5 Up!](https://html5up.net/ 'Site que contém modelos de sites prontos para você baixar')
7. [I hate systemd!](https://ihatesystemd.com/ 'Site que fala mal do systemd')
8. [LGPD: Lei Geral de Proteção a Dados](https://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/l13709.htm 'Página que contém a lei de proteção a dados direto da fonte, o artigo que regulamenta a tecnologia da informação é o 46')
9. [Luke Smith](https://lukesmith.xyz/ 'Site pessoal do Luke Smith')
10. [Meu teste de QI](https://www.testeqi.com.br/resultado-do-teste-de-qi/em-nosso-teste-de-qi-cezar-campos-obteve-126-teste--4806663--2022-11-23 'Resultado do meu teste de QI')
11. [Motherf\*cking website 2](http://bettermotherfuckingwebsite.com/ 'Isto é a p*rra de um site ainda melhor do que o primeiro')
12. [Motherf\*cking website 3](http://bestmotherfucking.website/ 'Uma versão ainda melhorada dos dois sites anteriores')
13. [Motherf\*cking website](http://motherfuckingwebsite.com/ 'Isto é a p*rra de um site')
14. [No Hello](https://nohello.net/ 'Site ensinando como mandar uma mensagem para uma pessoa')
15. [nosystemd.org](https://nosystemd.org/ 'Outro site que fala mal do systemd')
16. [O seu salário diante da realidade brasileira](https://www.nexojornal.com.br/interativo/2016/01/11/O-seu-sal%C3%A1rio-diante-da-realidade-brasileira 'Eu gosto de chamar este site de ”a calculadora da desigualdade”')
17. [RFC 822](http://www.faqs.org/rfcs/rfc822.html 'Padrão internacional para o formato dos endereços de e-mail')
