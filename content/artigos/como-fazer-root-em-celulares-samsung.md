+++
title = "Como fazer root em celulares Samsung"
date = "2020-03-03"
author = "Cézar Campos"
description = "Como fazer root em celulares da Samsung, um passo a passo para o pessoal que usa Linux no computador e não quer voltar para o Windows."
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado no
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/Como-fazer-root-em-aparelhos-Samsung/)

# Requerimentos

Existem várias formas de se fazer root nos smartphones e nesse artigo eu
abordarei o método Magisk, mas o que é Magisk?

# A máscara mágica para Android

O Magisk é uma suíte de ferramentas open source para Android que fornece o
acesso root, scripts e módulos que facilitam a customização do sistema. Ele te
fornece esses recursos se instalando na partição recovery do celular.

# Como faremos o Root

Basicamente o que vamos fazer é pegar o firmware original do smartphone,
remendar o Magisk nele e instalar de volta usando o Heimdall.

Alguns avisos antes de começar:

- Esse artigo destina-se a aparelhos Samsung lançados a partir de 2019.
- Fazer root em um aparelho Samsung causará perda da garantia e fará o Knox
  parar de funcionar, então prossiga por sua própria conta e risco.
- É necessário ter o bootloader desbloqueado, se não tiver leia:
  [como desbloquear o bootloader](/artigos/como-desbloquear-o-bootloader-em-celulares-samsung/)
- Você perderá os seus dados, então FAÇA BECAPE!

# Baixando os arquivos necessários

Faça download do seu firmware em qualquer um dos sites:

- [Samdb.Org](https://samdb.org/pt)
- [Sammobile firmwares](https://www.sammobile.com/firmwares/)

Descompacte o arquivo e envie o AP para o seu celular. Já no celular instale o
Magisk manager cujo apk poderá ser baixado aqui:

https://github.com/topjohnwu/Magisk#download

Se você pesquisar na internet você verá que o Magisk possui dezenas de sites,
não confie neles! Sempre baixe os artefatos da página do desenvolvedor no
Github.

Dentro do Magisk Manager vá em:

Instalar → Instalar → Selecionar e patchear um arquivo.

E selecione o arquivo AP que você acabou de passar do computador.

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura1.png" alt="Captura de tela do Magisk. Fonte: O autor (2020)" caption="Figura 1 - Captura de tela do Magisk">}}

Após finalizado o processo você verá uma tela assim:

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura2.png" alt="Terminal do Magisk mostrando o resultado da instalação. Fonte: O autor (2020)" caption="Figura 2 - Resultado da execução do Magisk">}}

Na pasta download do seu celular vai haver um novo arquivo chamado
"magisk_patched.tar" que contém o firmware modificado com o Magisk
pré-instalado. Passe este arquivo para o computador.

# Instalação do Magisk

Reinicie o celular em modo download e instale o _"magisk_patched.tar"_ no slot
AP se estiver usando o Odin junto com os outros arquivos BL, CP e HOME_CSC que
você baixou anteriormente.

Se você não tiver acesso ao Odin você pode descompactar todos os arquivos BL,
CP, HOME_CSC e magisk_patched.tar em arquivos de imagem, uma vez que tenha esses
arquivos você deve abrir o Heimdall-frontend e instalar cada um em uma partição
específica do celular. [Neste artigo](/artigos/como-usar-o-heimdall/) eu mostro
como fazer isso.

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura3.png" alt="Interface gráfica do Heimdall. Fonte: O autor (2020)" caption="Figura 3 - Interface gráfica do Heimdall">}}

Se optar pelo Heimdall ficará igual esta imagem e sim, é recomendável que você
instale o sistema todo e não somente os arquivos que o Magisk modificou, caso
contrário a sua partição de dados ( /data ) será encolhida.

Outra coisa, você deve desmarcar a opção "auto reboot" no Odin, caso esteja
usando o Heimdall marque a opção "no reboot".

Tudo certo, agora clique para começar a instalação!

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura4.png" alt="Heimdall instalando os arquivos no celular. Fonte: O autor (2020)" caption="Figura 4 - Heimdall instalando os arquivos no celular">}}

# Pós instalação

Agora temos o _Magisk_ instalado, porém ainda não podemos usar o aparelho.
Devemos fazer uma restauração de fábrica.

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura5.png" alt="Celular Samsung em modo de download. Fonte: O autor (2020)" caption="Figura 5 - Celular Samsung em modo download">}}

Saia do modo download segurando as teclas `Botão liga/desliga + Volume baixo`.

E imediatamente segure os botões para ir a recovery
`Botão liga/desliga + Volume cima`.

Segure por uns 3 ou 4 segundos.

Na tela de recuperação do aparelho procure a opção: **wipe data/factory reset**.

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura6.png" alt="Celular da Samsung em modo Recovery. Fonte: O autor (2020)" caption="Figura 6 - Celular Samsung em modo recovery">}}

Selecione "yes" para formatar o aparelho.

Agora podemos inciar o sistema normalmente, é normal que o aparelho reinicie
algumas vezes sozinho durante a primeira inicialização.

Depois que o aparelho ligar, preencha as configurações iniciais normalmente,
você deve ver o aplicativo do Magisk na tela inicial, se não estiver lá então
você deve instalar o APK manualmente igual você fez na página 1.

Quando você abrir o Magisk pela primeira vez ele deve pedir para fazer algumas
configurações extras, deixe ele fazer o seu trabalho e reiniciar o dispositivo
automaticamente.

E _voilà_ você tem o Magisk instalado!

{{< figure src="/artigos/como-fazer-root-em-celulares-samsung/figura7.png" alt="Aplicativo do Magisk aberto. Fonte: O autor (2020)" caption="Figura 7 - Status de instação Magisk">}}

É isso, espero ter esclarecido sobre como fazer root em aparelhos Samsung. Não
deixe de ler a documentação do Magisk e o fórum xda developers para mais
informações, o inglês lá é obrigatório.

# Referências

- [Página oficial do Magisk](https://topjohnwu.github.io/Magisk/)
- [Como fazer root no Galaxy A-20](https://forum.xda-developers.com/showpost.php?p=80021195&postcount=4)
- [Como fazer root em celulares Samsung no geral](https://topjohnwu.github.io/Magisk/install.html#samsung-system-as-root)
