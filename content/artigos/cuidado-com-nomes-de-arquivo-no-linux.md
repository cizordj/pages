+++
title = "Cuidado com nomes de arquivo no Linux"
date = "2019-08-22"
author = "Cézar Campos"
description = "Não consegui gravar um CD de músicas no Linux porque os nomes de artigos estavam com o nome estranho"
tags = ["Dica vivaolinux"]
language = "pt-br"
+++

E aí, galera do VOL, tudo certo?

Então, eu estava em casa tentando gravar um CD usando o _XFBurn_ para ouvir no
carro e quebrando a cabeça do por quê desse erro:

{{< figure src="/artigos/cuidado-com-nomes-de-arquivo-no-linux/figura1.png" alt="Caixa de diálogo dizendo que o arquivo de música não foi encontrado. Fonte: O autor (2019)" caption="Figura 1 - Mensagem de aviso do XFBurn" position="center" captionPosition="center" >}}

Eu não conseguia adicionar as músicas que queria gravar no XfBurn, esse erro
aparecia para todas as 30 músicas na minha pasta. É sério, eu tentei de tudo,
usando o "sudo", "chmod" na pasta, usar por linha de comando com aspas e nada
dessas músicas funcionar no programa.

Então, eu me toquei que o Kernel Linux interpreta as coisas de um jeito
peculiar, consegues notar o erro na foto acima?

É isso mesmo, o nome do arquivo possui um sustenido (ou hashtag) e isso
internamente transformava em comentário o que estava após o símbolo. O nome do
arquivo ia pela metade para o programa, por isso que o programa não achava o
arquivo.

A solução mais simples e óbvia de todas, é renomear os arquivos tirando fora
esse caractere e isso vale para tudo, não apenas música. Se você for editar uma
foto, vídeo ou qualquer outra coisa, esse erro pode aparecer.

{{< figure src="/artigos/cuidado-com-nomes-de-arquivo-no-linux/figura2.png" alt="Interface gráfica do aplicativo XfBurn mostrando todas as músicas selecionadas com sucesso. Fonte: O autor (2019)" caption="Figura 2" position="center" captionPosition="center" >}}

É isso, agora é só aproveitar o som!
