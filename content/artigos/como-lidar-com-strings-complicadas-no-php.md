---
title: "Como lidar com strings complicadas no PHP"
date: "2020-04-15"
author: "Cézar Campos"
description: "Como facilitar a leitura de código usando uma técnica chamada interpolação de variáveis"
tags: ["Dica vivaolinux"]
language: "pt-br"
---

Imagine que você é um programador _PHP_ e precisa fazer uma consulta no banco de
dados com um monte de variáveis, você começa a escrever a declaração SQL e
percebe que a string é muito complicada.

Pois é, essa é uma situação que passei recentemente e percebi que o código fica
muito bagunçado, se você não souber concatenar as strings de um jeito mais
limpo. Para você entender o drama, dê uma olhada no código abaixo:

```php
<?php
public function insert(Bill $bill, int $user_id): bool
{
  $link = $this->link;
  $billetBanking = $link->real_escape_string($bill->getBilletBanking());
  $sql =
    'INSERT INTO `bill` (`description`, `price`, `expirationDate`, `barCode`, `billetBanking`, `paid`) ' .
    "VALUES ('" .
    $bill->getDescription() .
    "', '" .
    $bill->getPrice() .
    "', '" .
    $bill->getExpirationDate() .
    "', '" .
    $bill->getBarCode() .
    "', " .
    "'" .
    $billetBanking .
    "', CONV('0', 2, 10) + 0)";
  $link->query($sql);
  return $this->addRelation($user_id);
}
?>
```

Eu quero você repare na variável `$sql`, olhe quantas linhas, pontos, aspas
duplas e simples, eu tive que adicionar para formar a variável final. É uma
parte do código difícil de olhar, pois é um balaio de gato, até mesmo um espaço
fora do lugar poderia parar o sistema todo e mostrar um erro.

Se eu imprimisse a variável, ela ficaria assim:

```sql
INSERT INTO `bill` (
    `description`,
    `price`,
    `expirationDate`,
    `barCode`,
    `billetBanking`,
    `paid`)
VALUES (
    'Conta de luz',
    '220',
    '2020-05-04',
    NULL,
    'um monte de dados binários aqui',
    CONV (
        '0', 2, 10) + 0);
```

Nada demais, esse era o resultado que eu queria chegar, porém como dizia o
Robert C. Martin em seu livro Clean Code, "nenhuma parte do código deve ser
ignorada", então volta e meia eu teria que voltar nessa parte para fazer alguma
manutenção.

Pois bem, o PHP processa todas as variáveis dentro de chaves {} em uma string,
não é necessário fechar a string para adicionar o valor com um ponto.

Outra coisa que me ajudou, é que ao quebrar as linhas de uma string, o PHP não
vai quebrar a linha no resultado final, para isso você tem que colocar "\n". Se
não colocar "\n", o PHP vai ajuntar tudo em uma linha só.

Exemplo:

```php
<?php
$msg = "
O
l
a

m
u
n
do";
echo $msg;
// Vai imprimir
// O l a m u n do
?>
```

No fim, a variável complicada ficou desse jeito aqui:

```php
<?php
$sql = "INSERT INTO \`bill\` (\`description\`, \`price\`, \`expirationDate\`,
        \`barCode\`, \`billetBanking\`, \`paid\`)
        VALUES ('{$bill->getDescription()}', '{$bill->getPrice()}',
        '{$bill->getExpirationDate()}', '{$bill->getBarCode()}', '{$billetBanking}',
        CONV('0', 2, 10) + 0)";
```

Versus versão anterior:

```php
<?php
$sql =
  'INSERT INTO \`bill\` (\`description\`, \`price\`, \`expirationDate\`, \`barCode\`, \`billetBanking\`, \`paid\`) ' .
  "VALUES ('" .
  $bill->getDescription() .
  "', '" .
  $bill->getPrice() .
  "', '" .
  $bill->getExpirationDate() .
  "', '" .
  $bill->getBarCode() .
  "', " .
  "'" .
  $billetBanking .
  "', CONV('0', 2, 10) + 0)";
```

Com isso, eu evitei os problemas de concatenar strings com o ponto e todo aquele
bolo de aspas duplas e simples.

Essa dica foi testada no PHP 7.3 no Debian Bullseye. Over and out!
