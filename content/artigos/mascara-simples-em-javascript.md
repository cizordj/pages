+++
title = "Máscara simples em Javascript"
date = "2019-09-14"
author = "Cézar Campos"
description = "Como fazer uma máscara em javascript puro sem jQuery. Descubra qual é a lógica por trás disso!"
tags = ["Dica vivaolinux"]
language = "pt-br"
+++

A fim de mostrar como criar uma máscara do zero, esta dica vai lhe mostrar como
fazer isso sem o _jQuery_. O propósito disso é mostrar a lógica de como uma
máscara em _javascript_ funciona e age nos formulários.

Neste exemplo vou mostrar como mascarar a entrada de uma placa de automóvel
padrão brasileiro ABC-1234 de modo que todos os usuários do seu sistema
preencham da mesma forma. Eu me deparei com essa situação enquanto fazia um
"mock application" de um sistema web voltado a área de logística e não tinha
nenhuma biblioteca javascript instalada.

Bom, comecemos então com o formulário em HTML.
{{< figure captionPosition="center" position="center" src="/artigos/mascara-simples-em-javascript/figura1.png" alt="Um campo de formulário em branco com a etiqueta “Insira a placa do carro”. Fonte: O autor (2019)" caption="Figura 1" >}}
Com esse código-fonte:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Placa</title>
  </head>
  <body>
    <form>
      <p>
        <label
          >Insira a placa do carro:
          <input
            type="text"
            name="placa"
            onkeyup="validarPlaca(this)"
            placeholder="ABC-1234"
            maxlength="8"
            autofocus />
          <!-- Ali o atributo "maxlength" evita que o usuário digite mais do que 8 caracteres -->
        </label>
      </p>
    </form>
  </body>
</html>
```

O input deve chamar a função javascript a cada digitação do usuário, então a
função se responsabilizará pela formatação do dado durante o preenchimento do
formulário.

Agora vamos implementar a função em javascript que vai manipular o dado de
acordo com o padrão que queremos (ABC-1234) e devolver ao formulário enquanto o
usuário digita.

Para isso foi criado uma função que transforma as três primeiras letras em
maiúsculas e adiciona um hífen automaticamente toda vez que o usuário termina de
digitar as três primeiras letras.

```html
<script>
  function validarPlaca(entradaDoUsuario) {
    var placa = entradaDoUsuario.value; // Passa para a variável 'placa' o que o usuário digitar no formulário

    if (placa.length === 1 || placa.length === 2) {
      // Quando a string possuir 1 ou 2 dígitos
      placaMaiuscula = placa.toUpperCase(); // Passa a string para letras maiúsculas
      document.forms[0].placa.value = placaMaiuscula; // Coloca a string modificada de volta no formulário
      return true;
    }

    if (placa.length === 3) {
      // Quando a string possuir 3 dígitos
      placa += '-'; // Adiciona um hífen
      placaMaiuscula = placa.toUpperCase(); // Passa a string para letras maiúsculas
      document.forms[0].placa.value = placaMaiuscula; // Coloca a nova string de volta no formulário
      return true;
    }
  }
</script>
```

Após adicionar esse trecho de código no seu arquivo html você poderá ver o
mascaramento em ação.

{{< figure captionPosition="center" position="center" src="/artigos/mascara-simples-em-javascript/figura2.gif" alt="Um pequeno GIF mostrando a placa do carro sendo formatada enquanto a pessoa digita no formulário. Fonte: O autor (2019)" caption="Figura 2 - Mascaramento em ação" >}}

É isso, eu usei o atributo onkeyup ao invés do onkeydown pois o último atrapalha
um monte na hora de apagar, caso o usuário cometa um erro de digitação ele pode
ficar frustrado ao perceber que não consegue apagar o que digitou XD. É bom
ficar ligado nisso também.

Até a próxima 😀
