+++
title = "Computador e placas antigas nvidia em pleno 2019"
date = "2019-2-08"
author = "Cézar Campos"
description = "Como sobreviver tendo um computador antigo e com a placa de vídeo da Nvidia usando o Linux em pleno ano de 2019."
tags = ["Artigo vivaolinux"]
+++

Originalmente publicado em
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/Computador-e-Placas-Antigas-Nvidia-em-Pleno-2019).

# Entendendo o problema

Os drivers de vídeo versão 304, 340 e 390, já são consideradas Legacy. Mas, o
que é um _driver Legacy?_

Um driver de vídeo _Legacy_ é aquele que não é mais suportado pelo time de
desenvolvedores, normalmente, são de GPUs da velha geração. Raramente eles
recebem alguma atualização, mas quando recebem, é só para compatibilizar com as
versões recentes do Kernel e do Xorg.

A cada versão considerada _Legacy_, uma lista enorme de placas de vídeos perdem
a garantia de que vão funcionar.

# Último lançamento

A última versão do 304 foi lançada em 2017 para o kernel mais recente, que na
época era o Linux 4.9 e o Xorg era 1.19, então, você tem que ter em mente que
essas são versões na qual você deve olhar em sua distro antes de usá-la.

Por isso que distros mais novas, como Manjaro, Debian Buster, Ubuntu 18.10 e
Linux Mint não funcionam direito e se funcionam, travam de vez em quando; isso
porque elas trazem por padrão versões mais novas do kernel e do Xorg e assim, os
drivers Legacy não funcionam nestas versões.

Além disso, distribuem com o driver de vídeo Nouveau. Neste caso, as distros
_Rolling Release_ não vão funcionar no seu velho computador, então, prefira as
distribuições _Fixed Release_.

Como exemplo, o Debian estável e seus derivados, para ter certeza de que o Xorg
e o Kernel não serão mexidos.

# Interfaces

Além disso, nem todos os gerenciadores de janela e interface são compatíveis,
das que testei, apenas XFCE e MATE funcionaram perfeitamente, as outras tiveram
algum bug. Cinammon, Gnome 3, KDE, Budgie e Deepin, são exemplos.

Então, temos que achar uma distro LTS com interface XFCE, ou MATE, para o nosso
velho computador.

# Procurando uma distro

Com essas informações, podemos procurar a distro ideal, como a minha favorita é
Debian estável, eu poderia mostrar como configurá-la. Mas, como não é amigável o
suficiente, então achei outra ainda mais fácil.

Descobri o _Zorin OS_ há pouco tempo e a julgar pela aparência e usabilidade, é
tão fácil quanto o Deepin, mas com a robustez do Debian e Ubuntu LTS.

Baixe a versão lite da ISO nesse site e escolha se quer pagar, ou não:

[Zorin OS - Página de download](https://zorinos.com/download/lite/)

{{< figure src="/artigos/computador-e-placas-antigas-nvidia-em-pleno-2019/figura1.png" alt="Fonte: O autor (2019)" caption="Figura 1 - Site de download do Zorin OS" >}}

Grave a ISO para algum pendrive e instale a distribuição no seu computador. Mas,
só um detalhe, durante a instalação, não se esqueça de marcar a opção: "Baixar
atualizações enquanto instala Zorin"

{{< figure src="/artigos/computador-e-placas-antigas-nvidia-em-pleno-2019/figura2.jpg" alt="Fonte: O autor (2019)" caption="Figura 2 - Tela de instalação do Zorin" >}}

Como o resto da instalação é intuitiva, então vamos pular para a pós-instalação.

# Primeira inicialização

Portanto, você acaba de instalar a distro e está inicializando-a pela primeira
vez.

A sua beleza no gráfico traz até esperança a computadores velhos. Quase não dá
pra acreditar que é uma interface XFCE de tão bem acabada, mas, infelizmente ela
não funciona bem assim "fora da caixa", então teremos que fazer umas
configurações extras.

{{< figure src="/artigos/computador-e-placas-antigas-nvidia-em-pleno-2019/figura3.png" alt="Fonte: O autor (2019)" caption="Figura 3 - Área de trabalho do Zorin OS" >}}

A primeira coisa, é instalar um kernel compatível com a _Nvidia_. Por padrão,
ele vem com a versão 4.15 genérica, então vamos instalar um kernel mais antigo.
Abra o terminal `Ctrl+Alt+t` e dê um update no _apt_:

```console
$ sudo apt update
```

E agora, instale o kernel lowlatency e seus headers:

```console
$ sudo apt install linux-image-lowlatency linux-headers-lowlatency
```

Depois de instalar o kernel, é recomendável que você desinstale o kernel atual
para você poder inicializar de vez no kernel lowlatency. Para isso, dê o
seguinte comando:

```console
$ sudo apt --purge remove linux-image-4.15* linux-headers-4.15* linux-modules-4.15*
```

Aparecerá uma mensagem dizendo, você está executando um kernel e tentando
remover a mesma versão e perguntará se você quer cancelar a remoção, diga que
NÃO!

{{< figure src="/artigos/computador-e-placas-antigas-nvidia-em-pleno-2019/figura4.png" alt="Fonte: O autor (2019)" caption="Figura 4 - Mensagem no terminal" >}}

Espere a desinstalação concluir e, por último, dê um _update_ no bootloader:

```console
$ sudo update-grub
```

A partir daí, sua nova missão é iniciar o sistema no kernel que você acabou de
instalar. Para isso, digite `reboot` no terminal e espere reinicializar.

# Segunda inicialização

Para termos certeza de que você inicializou com o kernel baixa latência, faremos
o teste. Digite no terminal e veja a saída que ele dá:

```console
$ uname -r
```

```
4.4.0-141-lowlatency
```

Se for parecida com essa, então você iniciou com o kernel certo.

Comecemos a instalação do bendito driver, invoque os poderes do apt com o
seguinte comando:

```console
$ sudo apt install nvidia-304
```

Isso irá instalar o driver na versão que está ali, junto também do
`build-essentials` que irá compilar todo o seu kernel baixa latência, a fim dos
drivers serem posto lá no meio. Se o texto final for semelhante a esse, então
você está pronto para continuar:

{{< figure src="/artigos/computador-e-placas-antigas-nvidia-em-pleno-2019/figura5.png" alt="Fonte: O autor (2019)" caption="Figura 5 - Término da instalação dos drivers da Nvidia" >}}

O próximo passo, é configurar o X, faça isso com um só comando:

```console
$ sudo nvidia-xconfig
```

Com o servidor X configurado, reinicie a máquina. 😉

# Conclusão

Após a reinicialização se tudo der certo, o GRUB deve iniciar somente no kernel
baixa latência, com os drivers de vídeo que você instalou.

Confira, após o login, se existe um aplicativo chamado Nvidia no menu de
aplicações e abra-o. Se tiver uma cara igual a essa abaixo, então parabéns, você
é o cara! Ou, a cara. rsrs

{{< figure src="/artigos/computador-e-placas-antigas-nvidia-em-pleno-2019/figura6.png" alt="Fonte: O autor (2019)" caption="Figura 6 - Tela após reinicialização do sistema operacional" >}}

Aproveite seu "novo velho computador", enquanto as distros ainda suportam ele
até 2021.

A simplicidade de um ambiente bonito, mesmo o seu hardware sendo antigo e mal
suportado em uma interface leve com o XFCE. Se você for audacioso(a), dá até
para jogar nesse estado.

É isso. Espero ter esclarecido o porquê das distros Linux não funcionarem de
primeira em algumas máquinas, principalmente com drivers de código fechado, que
não é obrigação das distribuições suportá-los. ;)

# Bibliografia

- O que é um driver legacy?

[What's a legacy driver?](https://www.nvidia.com/object/IO_32667.html)

- Requisitos mínimos de software para o driver Legacy:

[Chapter 2. Minimum software requirements](http://us.download.nvidia.com/XFree86/Linux-x86_64/304.137/README/minimumrequirements.html)

- Tabela com as versões de pacote no Ubuntu LTS (consultado a versão do Xorg):

[DistroWatch.com: Ubuntu](https://distrowatch.com/table.php?distribution=ubuntu)

- Tempo de suporte do Ubuntu LTS 16.04 (tempo até você comprar uma máquina
  nova):

[Ubuntu release cycle &lt;&lt; Ubuntu](https://www.ubuntu.com/about/release-cycle)

- Versões de lançamento Linux e suas datas:

[The Linux Kernel Archives - Release](https://www.kernel.org/category/releases.html)
