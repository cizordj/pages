+++
title = "Glossário de jargões da área de TI"
date = "2021-12-07"
author = "Cézar Campos"
description = "Termos da área de TI traduzidos para o português brasileiro"
tags = ["Utilidades", "Tecnologia"]
+++

Aqui você encontra jargões e termos técnicos da tecnologia da informação
traduzidos para o Português Brasileiro que você pode usar no dia a dia para se
comunicar melhor com os seus colegas ou escrever na internet. O presente
glossário tira proveito de um fenômeno chamado _aportuguesamento_ e todos os
termos aqui são dicionarizados. Em lugares onde o aportuguesamento não é
possível ─ por motivos de estranheza gramatical ou fonética ─ nós usamos uma
tradução livre.

## Bug

_Bug_ pode ser traduzido como uma falha ou uma anomalia. Normalmente nos
referimos a um bug quando um sistema de computador se comporta de uma maneira
inesperada.

_Ex.: Estou trabalhando para corrigir esta falha no sistema._

_Ex.: Foram corrigidas várias anomalias desde o último lançamento._

## Release

_Release_ pode ser traduzido como lançamento, soltura ou liberação. Soltura é
menos utilizado por ser uma palavra incomum no vocabulário do brasileiro, mas
não deixa de estar correto também.

_Ex.: O jogo ficou muito melhor de se jogar desde o último lançamento._

_Ex.: O software foi liberado com vários bugs._

## Hardware

As primeiras coisas que vêm à cabeça quando se fala em _hardware_ são
ferramentas para se consertar coisas em casa e/ou algum maquinário de fábrica,
como furadeira, serra elétrica, cortador de grama e chaves de cano. No contexto
da computação a palavra _hardware_ pode ser traduzida como “peças de
computador”.

Exemplos não recomendados:

_— O hardware anda muito caro ultimamente, não está dando para comprar nada._

_— O hardware do meu PC parou de funcionar._

_— Eu vou comprar um novo hardware para o meu computador._

Ao invés disso, prefira:

_— As peças de computador estão muito caras ultimamente, não está dando para
comprar nada._

_— O meu PC parou de funcionar._

_— Eu vou comprar uma peça nova para o meu computador._

Jamais coloque _hardware_ e **PC** na mesma frase pois o seu PC já é um
maquinário, dizer que _“O hardware do meu PC parou de funcionar”_ é o mesmo que
_“O maquinário do meu maquinário parou de funcionar”_ e isso configura
pleonasmo.
