+++
title = "Como customizar a sua ROM Android"
date = "2020-12-1"
lastmod = "2022-07-25"
author = "Cézar Campos"
description = "Como fazer para customizar a ROM você mesmo! Basicamente usando ferramentas que você pode instalar em qualquer distribuição GNU/Linux"
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado em
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/como-customizar-sua-rom-android)
em 1º de dezembro de 2020.

Eu dedico esse artigo aos entusiastas do mundo Android que gostam de mexer na
ROM de seus aparelhos, sejam bem vindos ao fórum Viva o Linux. Hoje eu vou
mostrar para vocês como customizar a ROM do seu Android de acordo com as suas
preferências através do computador. Viva o Linux!

## Resumo

Mostrarei para você como customizar a ROM original do celular com as ferramentas
que você pode encontrar na sua distribuição Linux. Todas as modificações do
Android serão feitas dentro do seu ambiente de desktop, antes de serem passadas
para o celular. A vantagem é que você vai aproveitar de toda a versatilidade que
o desktop tem para te oferecer nessas horas, além disso, para usufruir das
vantagens deste processo você não precisa ter root no aparelho, somente o
bootloader e o vbmeta desbloqueado.

NOTA 1: esse artigo se aplica a celulares da Samsung compatíveis com o projeto
Treble, lançados a partir de 2019 com Android 9 ou superior.

NOTA 2: Se o aparelho for novo, você perderá a garantia.

## Motivação

Você pode me perguntar, mas pra quê perder tempo fuçando no sistema do aparelho?
Bem, as razões são essas:

- Diversão;
- Liberar espaço;
- Bloquear ADS;
- Remover serviços do Google;
- Instalar APPs no sistema.

E é basicamente isso que nós vamos fazer hoje, nós vamos pegar a ROM original de
um Samsung Galaxy A20 e modificá-lo de acordo com as minhas necessidades, nesse
processo você aprenderá a fazer cada uma dessas etapas e poderá customizar a ROM
de outros aparelhos conforme você queira.

A ideia desse artigo não é desmoralizar e nem difamar as fabricantes de celular
pelo que elas mandam no aparelho, tampouco servirá de base para artigos
enviesados em prol do Software Livre, tudo aqui será didático e para fins de
aprendizado somente.

## Como vamos fazer

Basicamente, o que vamos fazer é pegar a stock ROM de um Galaxy A20 e fazer as
seguintes modificações:

- Remover todos os aplicativos inutilizados que vêm de fábrica;
- Instalar um arquivo hosts para bloqueio de propagandas;
- Remover alguns serviços do Google;
- Instalar a Aurora Store.

Para quem não sabe, a Aurora Store é uma alternativa em Software Livre para o
Google Play Store, ideal para quem não tem conta no Google e ao mesmo tempo quer
baixar aplicativos direto da Google Play. Além disso, a instalação do Aurora
Store no sistema vai servir de aprendizado para aqueles que querem saber como
uma loja de aplicativos funciona.

## Requisitos

Antes de começar, vale atentar aos requisitos que você precisa ter:

- Familiaridade com o terminal Linux;
- Um computador com Linux e espaço no HD;
- Alguma noção de Android.

Seu telefone precisa ter:

- O bootloader desbloqueado
- A partição VBMETA vazia

No final do artigo, eu entro em detalhes sobre como esvaziar o VBMETA sem fazer
root no aparelho.

## Ambiente

Não é necessário muita coisa para se customizar o Android no Linux, só algumas
ferramentas de linha de comando. Elas podem ser encontradas no pacote
"android-tools-fsutils" no Debian estável (Buster), Elementary ou Ubuntu 18.04:

```console
# apt-get install android-tools-fsutils
```

Com o ambiente preparado vamos começar a brincadeira.

## Como montar as partições do Android

A partições do Android são, em sua maioria, imagens esparsas de algum sistema de
arquivos EXT4. O Android, quando inicia, monta as partições a partir de blocos
que são arquivos de imagem somente leitura, elas permanecem assim pelo resto da
vida, a não ser que o fabricante torne a modificá-los através de alguma
atualização ou o usuário instale uma ROM customizada.

Algumas partições podem conter arquivos TAR, ao invés de um sistema de arquivos,
como mostrado no artigo:

- [Como modificar a tela do bootloader](/artigos/como-modificar-a-tela-do-bootloader-do-android)

Mas por quê as imagens são esparsas? A ideia por trás disso é economizar espaço,
visto que o armazenamento de memória nos celulares é muito limitado. Os Bytes
vazios são cortados e o tamanho da imagem só aumenta conforme o usuário adiciona
dados ao aparelho, consumindo apenas o espaço guardado nele, isso no caso da
partição de dados que é dinâmica.

Para montar uma imagem esparsa no Linux, você primeiro tem que converter ela
para uma imagem crua e depois montá-la. Note que depois da conversão, o arquivo
de imagem vai mostrar o seu real tamanho.

## Download da stock ROM

Primeiro, baixe a ROM original do seu aparelho no site da Samsung, pesquise pelo
modelo do celular, país e operadora. Os sites onde você pode baixar são esses,
mas podem haver muitos outros também:

- [Site oficial da Samsung](https://www.sammobile.com/firmwares/)
- ~~SamDB~~ (fora do ar)
- [SamFW](https://samfw.com/)
- [KFHost](https://kfhost.net/)

Após descompactar o arquivo ".zip", você vai ver que os arquivos terminam com
".tar".md5. Você pode verificar a integridade deles com essa dica:

- [Como verificar a integridade das stock ROMs da Samsung](/artigos/como-verificar-a-integridade-das-stock-roms-da-samsung)

Se decidir pular a verificação, então descompacte os arquivos com qualquer
programa de sua preferência. Se a ROM tiver comprimida com LZ4, instale o
seguinte pacote para descomprimi-lo:

```console
# apt install liblz4-tool
```

E depois para descomprimir:

```console
$ lz4 -d seu_arquivo_compactado.lz4
```

Após descomprimir todos os arquivos, você terá agora uma pasta cheia de
binários. Eu recomendo criar uma pasta separada só para guardá-los, para fins de
organização.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura1.png" alt="Uma imagem de uma pasta cheia de arquivos de imagem. Fonte: O autor (2020)" caption="Figura 1 - Listagem de arquivos da STOCK ROM do celular" position="center" captionPosition="center" >}}

Eu chamei esta pasta de "stock" pois é aonde vão ficar os arquivos originais sem
modificação, caso alguma coisa dê errado nós teremos um becape.

Crie uma outra pasta para guardar os seus arquivos modificados, afim de não se
perderem. Na imagem abaixo, pode-se notar que eu criei outra pasta chamada
"modified", pois é onde vão ficar os arquivos do Android modificado.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura2.png" alt="Uma pasta de computador aberto contendo duas pastas com o nome “modified” e “stock” respectivamente. Fonte: O autor (2020)" caption="Figura 2 - Pastas contendo as ROMs modificadas e originais" position="center" captionPosition="center" >}}

Com o terminal aberto nesta pasta, vamos converter a imagem do sistema Android
para um formato que o Linux "possa entender":

```console
$ simg2img ./stock/system.img ./modified/system.raw
```

Entre na pasta "modified" e monte a imagem do sistema como usuário root:

```console
$ cd modified
$ mkdir system
$ sudo mount ./system.raw ./system/
```

Agora, nós temos a imagem do sistema montada na pasta "system" do nosso
diretório de ROMs customizadas, dentro dela podemos fazer virtualmente qualquer
coisa.

Vamos montar a partição product:

```console
$ mkdir product
$ simg2img ../stock/product.img product.raw
$ sudo mount ./product.raw ./product/
```

Pronto, agora vamos começar a customização na próxima página.

## Como remover bloatwares

Primeiro, vamos começar removendo os _Bloatwares_. Para quem não sabe,
_Bloatwares_ são aplicativos que vêm instalados de fábrica e são impossíveis de
se remover pelo usuário, normalmente esses aplicativos nunca não usados e
consomem bateria e espaço no armazenamento interno.

Quando você liga um celular Android pela primeira vez, o sistema começa a
construir o cache do _Dalvik_ e instalar os aplicativos do sistema na partição
de dados, alguns aplicativos são apenas pacotes vazios que serão mais tarde
instalados pela PlayStore sem a interação do usuário.

Esses aplicativos são chamados de _STUB_ e você normalmente vai querer
removê-los nesse processo de _Debloat_.

Os aplicativos de sistema no Android são guardados nas pastas
_/system/priv-app/_ e _/system/app/_. Além dela, existem outras partições que
guardam aplicativos de terceiros e de operadoras como por exemplo as partições
hidden e product, nós abordaremos essas partições mais pro final. Vamos começar
removendo os aplicativos não essenciais da partição do sistema.

## Enxugando a partição do sistema

Entre na pasta "system" que você acabou de montar:

```console
# cd system
```

Liste os aplicativos:
{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura3.png" alt="Uma imagem de um terminal Linux aberto listando todas as pastas e arquivos na partição system do celular. Fonte: O autor (2020)" caption="Figura 3 - Listagem de arquivos pelo terminal" position="center" captionPosition="center" >}}

Note que na saída do comando acima, muitos aplicativos estão alojados na parte
privilegiada do sistema, muitos deles você consegue distinguir pelo nome e
outros são simplesmente difíceis de dizer, mesmo pesquisando na internet. Outros
fazem parte do coração do sistema e você simplesmente não pode removê-los, pois
se fizer isso o sistema poderá deixar de funcionar. Um bom exemplo disso é o
framework de pagamentos da Samsung!

Também note que ali existem alguns Stubs que farão o seu celular instalar
sozinho outros aplicativos que não vieram de fábrica, como por exemplo o
PreloadInstaller.

Se você já tem uma noção, você já pode começar removendo os APPs que você sabe
que não são essenciais, como por exemplo o instalador do Facebook:

```console
$ sudo rm -rfv "./system/priv-app/FBInstaller_NS/"
```

```
Removido './system/priv-app/FBInstaller_NS/FBInstaller_NS.apk'
Foi removido o diretório: './system/priv-app/FBInstaller_NS/'
```

Ou, o instalador de múltiplos Bloatwares que mencionei antes:

```console
$ sudo rm -rfv "./system/priv-app/PreloadInstaller/"
```

```
Removido './system/priv-app/PreloadInstaller/PreloadInstaller.apk'
Foi removido o diretório: './system/priv-app/PreloadInstaller/'
```

Eu compilei uma lista de todos os Bloatwares que removi do meu smartfone sem
quebrar o meu sistema, vou compartilhar com vocês para terem uma ideia:

{{< code language="text" title="Clique para ver a lista" >}}
./system/app/AutomationTest_FB ./system/app/BasicDreams ./system/app/BBCAgent
./system/app/BCService ./system/app/CarrierDefaultApp ./system/app/Chrome
./system/app/ChromeCustomizations ./system/app/DictDiotekForSec
./system/app/DuoStub ./system/app/EasymodeContactsWidget81
./system/app/EmergencyLauncher ./system/app/Facebook_stub
./system/app/FactoryCameraFB ./system/app/FBAppManager_NS
./system/app/Foundation ./system/app/GearManagerStub ./system/app/GameOptimizer
./system/app/GameOptimizingService ./system/app/GoogleCalendarSyncAdapter
./system/app/GoogleContactsSyncAdapter ./system/app/SplitSoundService
./system/app/SPrintSpooler ./system/app/Gmail2 ./system/app/GoogleTTS
./system/app/Maps ./system/app/MDMApp ./system/app/Netflix_activationCarriers
./system/app/Netflix_activationCommon ./system/app/Netflix_activationSamsung
./system/app/Netflix_stub ./system/app/PartnerBookmarksProvider
./system/app/PhotoTable ./system/app/PlayAutoInstallConfig
./system/app/SafetyInformation ./system/app/SamsungPassAutofill_v1
./system/app/SecFactoryPhoneTest ./system/app/SmartReminder
./system/app/SmartSwitchAgent ./system/app/Stk ./system/app/Stk2
./system/app/UniversalMDMClient ./system/app/WallpaperBackup
./system/app/WebManual ./system/app/WlanTest ./system/app/YouTube
./system/priv-app/AndroidAutoStub ./system/priv-app/BackupRestoreConfirmation
./system/priv-app/BeaconManager ./system/priv-app/BixbyService
./system/priv-app/BlockedNumberProvider ./system/priv-app/CallLogBackup
./system/priv-app/CarrierCodeChanger ./system/priv-app/CarrierConfig
./system/priv-app/ConfigUpdater ./system/priv-app/CSC
./system/priv-app/EasySetup ./system/priv-app/EmergencyInfo
./system/priv-app/EmojiUpdater ./system/priv-app/FBInstaller_NS
./system/priv-app/FBServices ./system/priv-app/Fmm ./system/priv-app/FotaAgent
./system/priv-app/GameHome ./system/priv-app/GameTools_Dream
./system/priv-app/KeyguardWallpaperUpdator ./system/priv-app/MateAgent
./system/priv-app/NSFusedLocation_v4.0 ./system/priv-app/OmaCP
./system/priv-app/OMCAgent5 ./system/priv-app/OneDrive_Samsung_v3
./system/priv-app/PhoneErrService ./system/priv-app/PaymentFramework
./system/priv-app/PreloadInstaller ./system/priv-app/SamsungBilling
./system/priv-app/SamsungDeviceHealthManagerService
./system/priv-app/SamsungDigitalWellbeing ./system/priv-app/SamsungPass
./system/priv-app/SecureFolder ./system/priv-app/SendHelpMessage
./system/priv-app/serviceModeApp_FB
./system/priv-app/SmartManager_v6_DeviceSecurity
./system/priv-app/SmartSwitchAssistant ./system/priv-app/SOAgent
./system/priv-app/TADownloader ./system/priv-app/TaPackAuthFw
./system/priv-app/Turbo ./system/priv-app/UltraDataSaving_O
./system/priv-app/UserDictionaryProvider ./system/priv-app/Velvet
./system/priv-app/WallpaperCropper ./system/hidden
./system/priv-app/GalaxyAppsWidget_Phone_Dream ./system/priv-app/Phonesky
./system/priv-app/Finder ./system/priv-app/GalaxyApps_OPEN
./system/priv-app/GameOptimizingService
./system/priv-app/GoogleExtServicesPrebuilt ./system/priv-app/GoogleFeedback
./system/priv-app/GoogleOneTimeInitializer
./system/priv-app/GooglePackageInstaller ./system/priv-app/GooglePartnerSetup
./system/priv-app/GooglePermissionControllerPrebuilt
./system/priv-app/GoogleRestore ./system/priv-app/GoogleServicesFramework
./system/priv-app/GmsCore ./system/priv-app/NSDSWebApp
./system/priv-app/SamsungAccount ./system/priv-app/SamsungCalendarProvider
./system/priv-app/SamsungCloudClient ./system/priv-app/SamsungExperienceService
./system/priv-app/SamsungSocial ./system/priv-app/SmartManager_v5
./system/priv-app/ThemeCenter ./system/priv-app/ThemeStore {{</ code>}}

É lógico que nem tudo nessa lista pode ser considerado um Bloatware, para
algumas pessoas, a pasta segura da Samsung (SecureFolder) pode ser útil. Além
disso, a remoção de alguns aplicativos pode causar algum efeito colateral, como
por exemplo o Knox parar de funcionar. A lista pode variar de aparelho para
aparelho e nem todos os nomes de aplicativos serão iguais a esses que eu listei,
então, tome cuidado ao usar listas pegas da internet feitas por terceiros.

Para usar essa lista é bem simples, coloque ela na raiz do sistema Android, na
pasta onde você montou o arquivo de imagem e rode o seguinte loop como usuário
root:

```console
# while IFS= read line
> do
> rm -rf "$line"
> done < bloatware_list.txt
```

Após este processo, a partição de sistema da sua ROM já deve estar alguns quilos
mais leve. O próximo passo agora é remover os aplicativos de outra partição que
normalmente vêm instalados de fábrica.

Ah, não esqueça de tirar a lista de dentro do sistema!

```console
# mv bloatware_list.txt ../
```

## Enxugando a partição do produto

Outro lugar que costuma ter aplicativos pré-instalados é a partição "product",
mas isso pode variar de celular para celular. Por sorte, o Galaxy-A20 costuma
trazer aplicativos ali, então eu vou mostrar como removê-los.

Entre na pasta "product", onde você montou o arquivo de imagem "product.raw"
anteriormente:

```console
# cd ../product
```

Dê um **ls**:

```console
# ls
```

```
app build.prop etc HWRDB lost+found omc priv-app sipdb
```

Como você pode ver, existe uma pasta "app" e uma "priv-app", o que quer dizer
que existem aplicativos ali. Remova todos os que você julgar necessário.

```console
# rm -rfv app/* priv-app/*
```

No meu, só tinha um manual de usuário, SamsungMax e o SamsungDTI.

```console
# rm etc/permissions/privapp-permissions-com.dti.samsung.xml
```

Também removi o arquivo que dava permissão para o SamsungDTI fazer mudanças no
aparelho.

## Enxugando a partição hidden

Outra partição que costuma ter APPs de terceiros é a partição "hidden", como o
Galaxy-A20 não possui essa partição, eu vou mostrar um exemplo pego de um
Samsung Galaxy J7 (2016).

```console
# ls ./hidden
```

```
Common_app INTERNAL_SDCARD lost+found
```

```console
# ls ./hidden/Common_app/
```

```
apps_brasil_v1.0.1_signed_aligned.apk
mcare-recarga-multi-samsung-v1.3.0-r3.apk SamsungAppsClube_v1.19.201.apk
smarttutor_b212.apk
```

```console
# rm ./hidden/Common_app/*
```

Como você pode ver, eu removi todos os APKs dentro da pasta "Common_app", isso
quer dizer que eles não existirão mais no aparelho. Com as partições devidamente
enxugadas, vamos para o próximo passo.

## Bloqueio de propagandas e malware

Todo mundo sabe que dá para bloquear propagandas através de um método clássico
no Unix, que é o bloqueio de DNS por arquivo hosts. No Android não é diferente,
por ser um derivado do Unix, você vai achar o arquivo "hosts" na barra etc do
sistema.

Entre no ponto de montagem do sistema:

```console
# cd ../system
# file etc
```

```
etc: broken symbolic link to /system/etc
```

O etc na raiz é um link simbólico para a pasta "etc" dentro da pasta "system".
Então, entre nela com o seguinte comando:

```console
# cd ./system/etc
```

E veja o conteúdo do arquivo hosts:

```console
# cat hosts
```

```
127.0.0.1               localhost
::1                     ip6-localhost
```

Bem simplesinho, não? Agora substitua ele com algum arquivo bem gordo de sua
preferência.

Um local de boa reputação para baixar um arquivo hosts é o repositório do
[StevenBlack](https://github.com/StevenBlack/hosts) no Github. Lá, você escolhe
as variantes do que deseja bloquear e pode baixar para o seu computador.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura4.png" alt="Uma captura de tela de uma página na web mostrando várias setas apontando para um link na qual contém o arquivo de hosts escolhido pelo autor para baixar. Fonte: O autor (2020)" caption="Figura 4 - Fonte dos hosts escolhida" position="center" captionPosition="center" >}}

Copie o link da variante desejada e cole no terminal para baixar.

```console
# rm hosts
# wget "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts" -O hosts
```

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura5.png" alt="Uma captura de tela de um terminal Linux contendo os comandos executados pelo autor e suas respectivas respostas. Fonte: O autor (2020)" caption="Figura 5 - Resultado da execução dos comandos" position="center" captionPosition="center" >}}

É isso, agora considere o seu sistema livre de propagandas (ou pelo menos, a
maioria delas).

## Instalando o Aurora Services

Se você tem o desejo de tirar o Google do seu smartfone, então essa página pode
interessar a você.

O [Aurora Store](https://gitlab.com/AuroraOSS) é um cliente para o serviço do
Google PlayStore que visa respeitar a sua privacidade ao máximo, ele oferece
atualizações e um serviço chamado "Aurora protect", que mostra quantos
rastreadores um aplicativo tem.

Este aplicativo é ideal para pessoas que se preocupam com o rastreamento e
querem usar alguma alternativa em software livre para a gigante das buscas em
seu smartfone.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura6.png" alt="Uma captura de tela do aplicativo Aurora Store contento uma vitrine de aplicativos que podem ser instalados no smartfone, semelhante à interface do Google play store. Fonte: O autor (2020)" caption="Figura 6 - Captura de tela do Aurora Store" position="center" captionPosition="center" >}}

O Aurora store pode instalar os aplicativos de vários jeitos, como usuário root,
sem root ou como aplicativo privilegiado do sistema (semelhante à forma que a
PlayStore utiliza). Aqui eu vou ensinar como instalar ele na parte privilegiada
do sistema.

Primeiro, vá até a página de releases do AuroraStore:

- [Releases · Aurora OSS / AuroraStore · GitLab](https://gitlab.com/AuroraOSS/AuroraStore/-/releases)

E baixe o último APK para o seu computador:

```console
# cd ../../../
# wget https://gitlab.com/AuroraOSS/AuroraStore/uploads/2b8a9544eb228170e5708ffb46cb55f5/AuroraStore_3.2.9.apk
```

Crie uma pasta dentro do "priv-app" do seu sistema:

```console
# mkdir -p ./system/system/priv-app/AuroraStore
```

E mova o APK para esta pasta:

```console
# mv AuroraStore_3.2.9.apk ./system/system/priv-app/AuroraStore/
```

Outro cliente que pode te interessar é o Aurora Droid que serve para baixar
aplicativos de software livre do f-droid.org:

- [Releases · Aurora OSS / AuroraDroid · GitLab](https://gitlab.com/AuroraOSS/auroradroid/-/releases)

Baixe o último APK do link acima e mova para a partição de sistema do seu
Android:

```console
# wget https://gitlab.com/AuroraOSS/auroradroid/uploads/58c94351a4b08d65e41501a1bf1ab1a4/AuroraDroid_1.0.6.apk
# mkdir ./system/system/priv-app/AuroraDroid
# mv AuroraDroid_1.0.6.apk ./system/system/priv-app/AuroraDroid/
```

Ainda não acabou, o Aurora Store precisa de mais um aplicativo para funcionar
completamente, ele é chamado de Aurora Services. Baixe o último APK do Aurora
Services nessa página:

- [Releases · Aurora OSS / Aurora Services · GitLab](https://gitlab.com/AuroraOSS/AuroraServices/-/releases)

```console
# wget https://gitlab.com/AuroraOSS/AuroraServices/uploads/c80bee54c1dc782df78a31065c710e59/AuroraServices-v1.0.6.apk
```

Crie uma pasta para ele na pasta "priv-app" do sistema e mova o APK para lá.

```console
# mkdir ./system/system/priv-app/AuroraServices
# mv AuroraServices-v1.0.6.apk ./system/system/priv-app/AuroraServices/
```

Nesse conjunto de aplicações, o Aurora Store e o Aurora Droid dependem do Aurora
Services para instalar e remover os aplicativos no sistema e para dar esse
privilégio, nós precisamos de um arquivo XML especial que vai ser lido pelo
sistema Android durante a inicialização, esse arquivo pode ser encontrado na
seguinte página do código-fonte do Aurora Services:

- [app/src/main/assets · master · Aurora OSS / Aurora Services · GitLab](https://gitlab.com/AuroraOSS/AuroraServices/-/tree/master/app/src/main/assets)

Baixe-o para o seu computador e mova para a seguinte pasta na partição do
sistema Android:

```console
# wget https://gitlab.com/AuroraOSS/AuroraServices/raw/master/app/src/main/assets/permissions_com.aurora.services.xml
# mv permissions_com.aurora.services.xml ./system/system/etc/permissions/
```

Agora vem a parte mais importante, o Android não vai reconhecer os aplicativos
se as permissões Unix deles estiverem erradas. As pastas devem ser 0755, os APKs
0644 e todos devem pertencer ao root, então antes de mais nada dê uma checada:

```console
# chmod 0755 ./system/system/priv-app/Aurora*
```

O comando acima vai aplicar o modo 0755 para todas as pastas do Aurora e o
comando abaixo vai aplicar o modo correto para todos os APKs do Aurora:

```console
# chmod 0644 ./system/system/priv-app/Aurora*/*.apk
```

E por último, só para garantir:

```console
# chown -R root ./system/system/priv-app/Aurora*
```

No fim, você vai acabar com 4 lojas de aplicativos no seu aparelho:

- O Google Play Store
- Galaxy Apps (da Samsung)
- Aurora Store
- Aurora Droid

Se você quiser, pode remover a Play Store com o seguinte comando:

```console
# rm -rf ./system/system/priv-app/Phonesky
```

E o Galaxy Apps com esse comando:

```console
# rm -rf ./system/system/priv-app/GalaxyApps_OPEN
```

E por que não remover o Google Play Services? Bom, muitos aplicativos dependem
do GMSCore para funcionar, exemplos são os aplicativos de bancos e aplicativos
do governo, além do mais as notificações push do Android precisam do Play
Services funcionando e atualizado, caso contrário, você não receberá as
notificações dos aplicativos.

Mesmo assim, se você tiver absoluta certeza de que não quer mais nenhum
resquício do Google, você pode removê-lo com o seguinte comando:

```console
# rm -rf ./system/system/priv-app/GmsCore/
```

A esse ponto do artigo, já devemos ter uma ROM bastante customizada e agora
livre dos serviços do Google.

## Como reempacotar a sua ROM

Agora que chegamos à reta final da nossa série de customizações Android, eu vou
mostrar para vocês como se faz para reempacotar a imagem do sistema e a imagem
da partição "product" que modificamos antes.

Como falei, a imagem que o Android usa são imagens esparsas; diferente das
partições normais do Linux, as imagens esparsas são somente-leitura e mais
compactas.

O pacote "android-tools-fsutils" nos provê um comando muito útil para esse caso.
Primeiro, desmonte as imagens do Android:

```console
# umount ./system/ ./product/
```

Certifique-se de ter desmontado bem a imagem do sistema Android, pois se houver
alguma queda de energia durante o processo de customização, o journal do sistema
de arquivos pode ser corrompido e você pode acabar causando um soft-brick no seu
smartfone na hora de instalar a custom ROM.

Agora, converta as imagens cruas para imagens esparsas:

```console
# ext2simg ./system.raw ./system_modificado.img
# ext2simg ./product.raw ./product_modificado.img
```

Confira se as imagens modificadas têm mais ou menos o mesmo número de chunks que
as imagens originais:

```console
# cd ..
# file ./modified/system.img
```

```
./modified/system.img: Android sparse image, version: 1.0, Total of 1038058 4096-byte output blocks in 139 input chunks.
```

```console
# file ./stock/system.img
```

```
./stock/system.img: Android sparse image, version: 1.0, Total of 1054720 4096-byte output blocks in 27 input chunks.
```

```console
# file ./modified/product.img
```

```
./modified/product.img: Android sparse image, version: 1.0, Total of 100764 4096-byte output blocks in 10 input chunks.
```

```console
# file ./stock/product.img
```

```
./stock/product.img: Android sparse image, version: 1.0, Total of 102400 4096-byte output blocks in 17 input chunks.
```

Isso é bom sinal, quer dizer que as imagens vão funcionar. Se alguma imagem
tiver um número absurdo de input chunks, como por exemplo mais de "1000 input
chunks", repita o processo, isso quer dizer que a imagem do sistema foi criada
com os parâmetros errados pelo ext2simg.

Isso pode acontecer caso você cancele a desmontagem ou corrompa o journal do
sistema de arquivos.

## Resultado e instalação

Agora que temos a nossa custom ROM, vamos instalar ela no smartphone usando o
Heimdall, para quem não sabe, o Heimdall é uma ferramenta nativa do Linux que
serve para instalar ROMs nos celulares da Samsung.

Para mais detalhes, veja:

- [Introdução ao Heimdall](/artigos/introducao-ao-heimdall)
- [Como usar o Heimdall](/artigos/como-usar-o-heimdall)

Nos tutoriais acima, eu ensino a compilar o Heimdall direto da fonte, mas se
você estiver usando o Debian testing ou superior, você já pode instalá-lo direto
dos repositórios.

```console
# apt-get install heimdall-flash
```

Agora, para podermos instalar a custom ROM, precisamos desbloquear o bootloader
do celular. Caso não tenha desbloqueado o bootloader, eu recomendo que siga esse
tutorial:

- [Como desbloquear o bootloader em celulares Samsung](como-desbloquear-o-bootloader-em-celulares-samsung)

Além disso, precisamos de um vbmeta vazio para instalar no seu aparelho, sem ele
o smartphone vai disparar o seguinte erro:

        Only official binaries are allowed to be flashed

Por quê isso acontece? Porque o sistema Android quando é inicializado, verifica
a assinatura da ROM para ver se ela é genuína e caso você instale alguma coisa
que não seja a original da fabricante, você será levado para a tela de download
toda vez que ligar o smartphone, o único jeito de corrigir este erro é
reinstalando a stock ROM completa do seu aparelho.

Para burlar isso você, deve instalar um arquivo vazio na partição VBMETA logo
depois de desbloquear o bootloader, pois assim você garantirá que o aparelho não
vai colocar impeditivos na hora de customizar a ROM.

Para conseguir o arquivo "vbmeta.img" compatível com o seu smartphone, siga o
tutorial do link abaixo:

- [Como fazer root em celulares Samsung](/artigos/como-fazer-root-em-celulares-samsung)

Porém, não instale nada ainda, apenas pegue o arquivo "magisk_patched.tar"
proveniente daquele tutorial, passe para o computador, extraia o arquivo
"vbmeta.img" e coloque-o na pasta "modified", onde você guarda as suas ROMs
customizadas.

É dessa forma que você vai instalar a custom ROM sem precisar fazer root no
aparelho. Se você já fez root usando o método Magisk, então você já tem a
partição VBMETA vazia e pode pular esta etapa.

A princípio, tudo está preparado para instalarmos a custom ROM no aparelho.
Desligue o celular, pressione os dois botões de volume ao mesmo e ligue ao
computador para entrar no modo download.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura7.png" alt="Fotografia de um celular da Samsung em modo download ligado a um computador através de um cabo USB. Fonte: O autor (2020)" caption="Figura 7 - Celular em modo download conectado ao computador" position="center" captionPosition="center" >}}

Veja se o Heimdall detecta o aparelho antes de começar:

```console
# heimdall detect
```

```
Device detected
```

E agora instale a custom ROM com o seguinte comando:

```
# heimdall flash --pit ../stock/A20_SWA_OPEN.pit --VBMETA vbmeta.img --SYSTEM system_modificado.img --PRODUCT product_modificado.img
```

Onde:

- `--pit` (vai o arquivo "pit" que veio junto com os arquivos da stock ROM);
- `--VBMETA` (aqui vai o arquivo "vbmeta" vazio produzido pelo Magisk);
- `--SYSTEM` (a imagem de sistema da sua custom ROM);
- `--PRODUCT` (a imagem da partição "product" que você modificou antes);

Agora o seu smartfone já deve iniciar com a ROM customizada. :)

Para o melhor resultado, entre no modo recovery, formate o cache Dalvik e faça a
restauração de fábrica. É normal que a primeira inicialização demore, pois ele
estará construindo o cache de novo e instalando os aplicativos do sistema, isso
pode variar de uns 5 a 10 minutos.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura8.png" alt="Fotografia de um celular da Samsung sobre uma mesa de madeira conectado a um computador através de um cabo USB exibindo a logo da Samsung na tela dando a entender que o celular está inicializando o sistema Android" caption="Figura 8 - Celular reiniciando" position="center" captionPosition="center" >}}

Após passar as configurações iniciais do aparelho, você poderá notar o quão
limpo ele deve estar.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura9.png" alt="Captura de tela mostrando a grade de aplicativos do celular praticamente vazia. Fonte: O autor (2020)" caption="Figura 9 - Grade de aplicativos após a primeira inicialização" position="center" captionPosition="center" >}}

A maioria dos aplicativos você mesmo terá que instalar, mas antes abra o Aurora
Services, dê as permissões para ele e toque no Whitelist.

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura10.png" alt="Captura de tela mostrando a interface do Aurora Services. Fonte: O autor (2020)" caption="Figura 10 - Aurora Services" position="center" captionPosition="center" >}}

Marque o Aurora Store e o Aurora Droid para permitir que eles instalem
aplicativos no sistema.

Agora, abra o Aurora Store, passe as configurações iniciais e vá em
"Configurações". Depois vá em "Instalações", mude o método de instalação para:
Aurora Services

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura11.png" alt="Captura de tela mostrando uma lista com os métodos de instalação do aurora store: Nativo, Super usuário e Aurora Services. Fonte: O autor (2020)" caption="Figura 11 - Métodos de instalação do Aurora Store" position="center" captionPosition="center" >}}

Faça o mesmo para o Aurora Droid e pronto! Agora você está livre para instalar
aplicativos no celular.

## Considerações finais

Só para constar, a ROM do meu aparelho ficou quase 1GB mais leve depois do
processo de remoção de APPs, como você pode ver nas imagem abaixo:

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura12.png" alt="Uma captura de tela mostrando que o tamanho do arquivo de imagem do sistema Android está em torno 4 Gigabytes. Fonte: O autor (2020)" caption="Figura 12 - Propriedades do arquivo de imagem do sistema Android" position="center" captionPosition="center" >}}

Este é o tamanho da imagem original do sistema, enquanto que esse é o tamanho da
ROM customizada:

{{< figure src="/artigos/como-customizar-a-sua-rom-android/figura13.png" alt="Uma captura de tela mostrando que o tamanho do arquivo de imagem do sistema Android modificado está em torno de 3 Gigabytes. Fonte: O autor (2020)" caption="Figura 13 - Propriedades do arquivo de imagem customizado do Android" position="center" captionPosition="center" >}}

Além disso, a bateria está durando até dois dias, já que não há nenhum serviço
de sincronização em nuvem instalado. Por outro, lado a conveniência de se ter
becapes automáticos para TUDO, foi sacrificada. Então, toda vez que eu for
instalar uma nova versão da ROM, um becape manual deverá ser feito.

Perceba também que não fizemos ROOT no aparelho, por essa razão o smartfone
aparecerá como "limpo" para aplicativos que detectam isso e o sistema será
marcado como seguro.

É isso pessoal, espero que tenham curtido esse método de customização de ROMs e
fica o aviso: O Viva o Linux e o autor se eximem da responsabilidade pela perda
de celulares através da modificação do firmware.

Siga este procedimento por sua própria conta e risco.

Over 'n out.
