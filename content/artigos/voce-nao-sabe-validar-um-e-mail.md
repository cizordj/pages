+++
title = "Você não sabe validar um e-mail"
date = "2023-01-18"
lastmod = "2023-04-05"
author = "Cézar Campos"
description = "Você é um tolo de pensar que sabe validar um endereço de e-mail, e eu não estou brincando. A maioria dos sites em atividade atualmente validam o e-mail de forma errônea e por conta disso, os usuários sofrem as consequências!"
tags = ["Desabafos", "Tecnologia"]
+++

Você é um tolo de pensar que sabe validar um endereço de e-mail,
e eu não estou brincando. A maioria dos sites em atividade atualmente
validam o e-mail de forma errônea e por conta disso, os usuários sofrem
as consequências!  Um e-mail mal validado causa problemas de privacidade
e até segurança.

## Exemplos de validações precárias

Primeiramente, vou dar alguns exemplos de validações de e-mail
que encontrei em sites e aplicações que utilizo.  Além disso, vou
explicar por que essas regras são inadequadas e como elas prejudicam
a experiência e a segurança do usuário.

{{< figure alt="Caixa de diálogo de um website dizendo que o e-mail está incorreto ou que possui um formato inválido contendo um botão rosa com a seguinte legenda: Digitar e-mail novamente. Fonte: O autor (2022)" caption="Figura 1 - Validação de e-mail no site Serasa Consumidor" src="/artigos/voce-nao-sabe-validar-um-e-mail/email_serasa.png" position="center" captionPosition="center" >}}

O site do Serasa Consumidor não aceitou o meu endereço de e-mail.
Além disso, não consegui alterar o endereço diretamente na API
devido a uma proteção de CSRF[^1], o que me deixou totalmente à mercê
da validação feita pelo Javascript.  Ademais, entrei em contato com o
suporte para realizar a alteração, porém o processo é extremamente
burocrático.

[^1]: _Cross-Site Request Forgery_ foi a ação que tomei para tentar
mudar o meu e-mail sem depender do formulário.

{{< figure alt="Recorte de tela mostrando o conteúdo da mensagem do suporte. Fonte: O autor (2022)" caption="Figura 2 - Retorno do suporte à solicitação de troca de e-mail" src="/artigos/voce-nao-sabe-validar-um-e-mail/serasa_email_one.png" position="center" captionPosition="center" >}}

Apesar de deveras **ridículo**, fiz o que ela pediu e enviei as fotos dos
documentos necessários, juntamente com uma imagem mostrando a mensagem
de erro. O retorno que eu obtive foi esse.

{{< figure alt="Recorte de tela mostrando o conteúdo da mensagem do suporte. Fonte: O autor (2022)" caption="Figura 3 - Retorno do suporte após a confirmação da minha identidade" src="/artigos/voce-nao-sabe-validar-um-e-mail/serasa_email_two.png" position="center" captionPosition="center" >}}

Ora, qual é o problema em usar um provedor “menos conhecido”? Por que
um domínio brasileiro não seria confiável para eles? Por que eles só
aceitam os provedores mais comuns como Gmail, Hotmail e etc.? A resposta
é bem simples:

**Eles querem e-mails pessoais!**

Endereços de e-mail com domínios personalizados e corporativos
não possuem nenhum valor no mercado negro de dados pessoais, porque
esses endereços não pertencem a nenhuma pessoa física, mas a uma
organização com políticas de segurança e rotatividade de usuários.
Além disso, pessoas com conhecimentos em tecnologias dificilmente usariam
seus endereços pessoais em aplicações tirânicas como as governamentais
ou a do Serasa.  Foi por isso que eles recusaram o endereço do meu
e-mail, pois é muito seguro para ser armazenado em sua base de dados,
e eles sabem que eu poderia processá-los por danos morais caso haja
vazamento de informações devido a uma brecha de segurança.

Em resumo, além da frustração de ter que tirar foto e enviar todos os
meus documentos, não consegui trocar o e-mail no meu cadastro devido
a uma validação de e-mail mal programada por parte do desenvolvedor
responsável.

Outro exemplo...

{{< figure alt="Recorte de tela dizendo que o endereço de e-mail deve possuir um formato específico como nome arroba exemplo ponto com ponto br. Fonte: O autor (2022)" caption="Figura 4 - Validação de e-mail no Mercado Livre" src="/artigos/voce-nao-sabe-validar-um-e-mail/email_mercado_livre.png" position="center" captionPosition="center" >}}

Não preciso nem dizer que esta é uma regra de validação idiota.
Primeiro, não é com um simples _regex_ que se valida um endereço
de e-mail.  O programador provavelmente jogou no Google e pegou o
primeiro resultado que apareceu. Segundo, nem todo endereço segue este
mesmo formato.  O meu mesmo não seguia e, por isso, eu tive que abrir
um chamado no suporte para fazer a alteração manualmente.  Felizmente a
equipe do Mercado Livre foi razoável e alterou o meu e-mail.

{{< figure alt="Recorte de tela mostrando que um endereço de e-mail deveria possuir até 150 caracteres. Fonte: O autor (2022)" caption="Figura 5 - E-mail com limitação de tamanho" src="/artigos/voce-nao-sabe-validar-um-e-mail/regra_de_email_retardada.png" position="center" captionPosition="center" >}}

Este é mais um exemplo de um site no qual me cadastrei que limitava
o tamanho de caracteres no meu endereço de e-mail. Não me recordo
exatamente qual site era, mas lembro de ter capturado a tela apenas para
o propósito deste artigo que estás lendo agora. Eu até entendo que
há um campo em um banco de dados por trás disso que impõe um limite
de 150 caracteres, porém quem já leu o mínimo das especificações
sabe que o limite para o tamanho de um endereço de e-mail é de 254
caracteres, incluindo o arroba. Mais tarde entrarei em detalhes sobre
quais especificações são essas e por que é importante que todo
programador as leia para se tornar digno da profissão.

{{< figure alt="Captura de tela do aplicativo Instagram para Android exibindo uma mensagem de endereço inválido. Fonte: O autor (2023)" caption="Figura 6 - Validação de e-mails pelo aplicativo do Instagram para Android" src="/artigos/voce-nao-sabe-validar-um-e-mail/validacao_app_instagram.png" position="center" captionPosition="center" >}}
{{< figure alt="Tela de cadastro da plataforma de jogos steam dizendo que o endereço de e-mail digitado é inválido. Fonte: O autor (2023)" caption="Figura 7 - Validação ao criar conta na Steam" src="/artigos/voce-nao-sabe-validar-um-e-mail/steam_validation.png" position="center" captionPosition="center" >}}

Nem mesmo as "big techs" escapam da questão dos endereços de
e-mail.
Felizmente, para minha sorte, o Instagram web não valida o
endereço da mesma forma que o aplicativo de celular.
Consegui fazer a alteração facilmente acessando pelo computador.
O mesmo aconteceu com a minha conta na Steam.
Consegui fazer a alteração após criar
a conta.
De alguma forma, o formulário de alteração não valida o
endereço da mesma forma que o de criação.

Eu poderia passar horas e horas aqui mostrando exemplos de validações
toscas em websites comuns que todos encontram pela internet.
No entanto, o propósito deste artigo é alertar os desenvolvedores sobre a importância
de estudar segurança da informação e questões de privacidade, além de
pensar na experiência do usuário.
Por causa de uma validação boba,
muitos usuários precisam abrir chamados para a equipe de suporte,
criando mais transtornos para todo mundo.

## Por que isso acontece?

Uma das razões para isso acontecer na maioria dos sites é a
"_politicagem_".
Normalmente, os donos desses sites querem que as
pessoas sejam identificadas de tal forma que não possam criar contas
duplicadas. Além disso, pedem a implementação de outros mecanismos
de identificação, que vão desde o número de telefone até a
identificação por CPF. É por causa desse requisito que as pessoas
são forçadas a usar o endereço de e-mail pessoal nesses websites e
não podem usar o mesmo endereço para criar novas contas.

Em teoria, é possível criar um número infinito de contas em um site
usando um único endereço de e-mail, e isso não é algo desejável para
os donos de websites, pois não agrega valor às suas bases de dados. Uma
base com e-mails únicos vale muito mais do que uma base com e-mails
repetidos. Por isso, ao lidar com dados pessoais, lembre-se da máxima:

    O dado é o novo petróleo

Esta forçação de unicidade faz com que os usuários sejam obrigados
a fornecer seus dados pessoais mais sensíveis, como informações de
localização, documentos pessoais com foto, autoretratos segurando placas
assinadas e até mesmo impressões digitais! Tudo isso para dar mais valor
e veracidade à conta vinculada, tudo para "segurança" do usuário. Eles
não confiam em você na hora de trocar um endereço de e-mail, mas
é esperado que você confie que eles vão manter seus dados pessoais
seguros e não fazer nada de errado com eles, principalmente com as fotos
que você tirou no formato de _selfie_ para a equipe de suporte avaliar.

A segunda maior razão é a inocência misturada com ignorância dos
desenvolvedores, muitos sequer sabem programar tampouco se espera que tenham
conhecimento em privacidade, legislação e direito digital. O maior erro
dos tecnólogos da computação foi desvincular a filosofia da ciência,
o que tem levado a uma caminhada cega rumo a um mundo ditatorial, como
descrito no livro "1984" de George Orwell.

{{< figure alt="Captura de tela de uma mensagem de e-mail contendo o seguinte conteúdo: Entendi, Cézar. Então sinto muito que você tenha conseguido fazer esse cadastro, mas assim como a maioria das outras empresas no nosso sistema esse e-mail não é aceito, tá? Não temos como fazer a liberação desse modo, entende? Salva Vidas fluke. Fonte: O autor (2023)" caption="Figura 8 - Justificativa da Fluke" src="/artigos/voce-nao-sabe-validar-um-e-mail/fluke_support.png" position="center" captionPosition="center" >}}

Eis a prova.

## O que há de “errado” com os meus e-mails

Aposto que você ficou curioso(a) em saber quais foram os endereços de
e-mail que não passaram na validação desses websites. Pois bem, nada do
que eu vou mostrar é novidade, e também nenhum dos meus endereços são
inválidos. Todos os e-mails que eu coloquei possuíam um comentário,
nada demais. Se você está na internet há mais de 20 anos, provavelmente
já ouviu falar desse truque, que faz com que o próprio endereço de
e-mail denuncie qual foi o website que vazou.

Estou falando de um simples comentário com um sinal de adição (+)
anexado. Para cada conta que crio em um website, eu anexo o domínio no
meu endereço de e-mail e também um comentário contendo meu nome de
usuário naquele site. Vou lhe dar um exemplo.

Supondo que o meu e-mail seja _usuario@exemplo.com_ e eu vou me
cadastrar na loja **silva.com.br**, o meu endereço de cadastro será:
_usuario+silva.com.br@exemplo.com_. Isso não obrigará a criar uma nova
conta de e-mail com o sufixo _+silva.com.br_, mas fará com que todos os
e-mails sejam enviados para a conta original. Caso o website possua um
sistema de _usernames_, eu costumo cadastrar a URL completa do meu perfil
junto com o e-mail: _usuario+silva.com.br/nome-de-usuario@exemplo.com_.

Só isso garante que haverá mais segurança ao me cadastrar, pois sabemos
que quando ocorrer um grande vazamento de dados, o endereço de e-mail
entregará o domínio do site invadido.

## O jeito certo de validar um e-mail

Vou mostrar aqui o jeito correto de validar um e-mail usando algumas das
linguagens mais comuns encontradas na web. Primeiramente, é importante
que você entenda que existem diversas especificações que determinam
como um endereço de e-mail deve ser escrito, e as RFCs 822 até a 5952
tentam padronizar o formato, mas as mais importantes são a primeira e
a última. Há muitas RFCs e eu não vou citar todas, vou me concentrar
apenas na mais antiga, que é a RFC 822.

A RFC 822 é a especificação mais antiga para endereços de e-mail
e, por ser a mais antiga, também é a mais usada por linguagens de
programação sérias. Se você quer que o e-mail seja o mais compatível
possível, prefira uma abordagem mais conservadora e permita que os
e-mails sejam validados de acordo com essa especificação. Agora,
se você é um pouco mais progressista, pode validar os endereços de
acordo com a última RFC e permitir que os mais variados caracteres
preencham o seu banco de dados.

Se todos os sites que eu mencionei levassem a especificação mais a sério
eu não precisaria estar escrevendo este artigo. Todos os sites que eu
citei acima não contemplam nem a primeira especificação, o que é uma
**vergonha** para os desenvolvedores.

A forma mais comum de se validar um e-mail em todas as linguagens de
programação é por meio de _expressões regulares_. Se a sua
linguagem possuir um método nativo de validação, então prefira este método.
Como a linguagem PHP por exemplo:

```php
<?php
function validateEmail(string $email): bool
{
  return false !== filter_var($email, FILTER_VALIDATE_EMAIL);
}
```

Outras linguagens como o Javascript também possuem validação nativa,
graças ao HTML5.

```html
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <form>
      <input type="email" required />
      <input type="submit" />
    </form>
  </body>
</html>
```

Não é preciso instalar bibliotecas e utilizar expressões regulares
malfeitas como nos exemplos apresentados no início do artigo. É
possível normalizar os endereços de e-mail tanto no back-end quanto no
front-end, convertendo-os para letras minúsculas, ou restringir o uso
de determinados domínios, sem, no entanto, impedir que os usuários
utilizem endereços personalizados.

Você pode utilizar o endereço de e-mail
**usuario+silva.com.br/nome-de-usuario@exemplo.com** nestes
dois exemplos de código e perceber que isso será validado sem nenhum
problema.

Caso necessite validar de acordo com outros padrões além da RFC 822, aí sim
você pode recorrer a bibliotecas de terceiros, desde que sejam bibliotecas
de boa reputação e não desenvolvidas por um zé roela estudante de computação.

## Palavras finais

Como você pôde ver, qualquer pessoa consegue validar endereços de
e-mail, se você não consegue, então você precisa se aprimorar. Como
desenvolvedor, você é responsável pela segurança e bem-estar dos
usuários. É por causa de pessoas que não levam essa responsabilidade
a sério que temos aplicações que restringem a liberdade dos
usuários sobre seus dados. E quem permite isso acontecer é **você**,
desenvolvedor. Portanto, sinta-se responsável por todas as dores causadas
pela ditadura tecnológica, já que é você quem possibilita isso.

---
## Links úteis
- [RFC - 822](https://www.rfc-editor.org/rfc/rfc822)
- [RFC - 3696 - página 6](https://www.rfc-editor.org/rfc/rfc3696#page-6)
- [RFC - 5321 - página 63](https://www.rfc-editor.org/rfc/rfc5321#page-63)
- [EmailRegex ponto com](https://emailregex.com/)
- [Eu sabia como validar um endereço de e-mail até ler a RFC!](https://haacked.com/archive/2007/08/21/i-knew-how-to-validate-an-email-address-until-i.aspx/)
- [Filtros de validação do PHP](https://www.php.net/manual/en/filter.filters.validate.php)
