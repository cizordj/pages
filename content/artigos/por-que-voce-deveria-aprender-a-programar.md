+++
title = "Por que você deveria aprender a programar"
date = "2020-01-20"
author = "Cézar Campos"
description = "Nesse artigo eu abordo questões sociais do porquê as pessoas deveriam saber programar."
tags = ["Estilo de Vida", "Tecnologia"]
+++

Eu estou escrevendo este artigo sozinho no meu quarto à noite enquanto escuto um
bom jazz.

Bom vamos ao que interessa, por que você deveria saber programar? A resposta é
bem simples, você deixa de ser um consumidor de software e passa a fabricar os
seus próprios aplicativos. A parte complicada é que eu estou dizendo isso para
pessoas normais e não profissionais de TI, já é de se esperar que todo
profissional de TI saiba programar, mas isso não é o caso, o que eu estou
dizendo é que: Pessoas normais deveriam saber programar.

A programação de computadores não é só uma atividade profissional como também
deveria ser matéria obrigatória nas escolas, as pessoas deveriam aprender a
fazer macros, aprender banco de dados, aprender a montar planilhas de Excel e o
mais importante, aprender linha de comando. A programação estimula a lógica e
previne o cérebro contra doenças degenerativas como o Alzheimer, você já viu um
programador com Alzheimer? A programação de software ajuda a estimular o
crescimento pessoal e a colaboração entre os indivíduos, você como programador
aprende a pensar não só em termos de lógica de computação como também em termos
de contribuição social. Todo software que você desenvolve busca resolver algum
problema social.

## O que há de errado com não-programadores?

Pessoas não programadoras tendem a ser mais consumistas e chatas, quando elas
utilizam algum software elas esperam que ele faça tudo para elas, elas querem
que o software resolva todos os problemas e seja simplesmente perfeito! O
software não pode ter bugs, deve ter uma interface agradável e ter um monte de
recursos que na maioria dos casos as pessoas nem utilizam, além do mais o
software deverá ser gratuito e não pode incluir propagandas pois propagandas são
intrusivas. As lojas de aplicativos também são culpadas por estimular tal
comportamento, pois cada vez que você desinstala um aplicativo a loja enche a
sua tela com sugestões de "aplicativos similares" fazendo com que as pessoas
adquiram o hábito de classificar mal os aplicativos manchando a reputação do
desenvolvedor e instalar vários outros afim de achar aquele que "faz tudo".

Isso é típico comportamento de consumista que acha que programação é uma coisa
intuitiva. **Não seja esse tipo de pessoa**. Ao invés disso, mande um e-mail
para a pessoa que desenvolveu o aplicativo dando uns parabéns, seja grato pelos
softwares que você pode usar gratuitamente e seja mais grato ainda se ele for
livre.

## De consumidor a produtor

Em tudo nessa vida existem pessoas que consomem e aquelas que produzem, não seja
consumidor, seja produtor. Não jogue videogames, fabrique os jogos! Enquanto
você estará produzindo outros estarão consumindo, ser um produtor de software
não é só programar uma aplicação dentro de uma empresa e sim todas as aplicações
que você usa no dia a dia e compartilha com outros para poder usar também. Veja
o conceito de [hacker](https://www.stallman.org/articles/on-hacking.html).

Quando uma pessoa aprende a programar ela não só aprende os conceitos básicos de
computação como também aprende a se colocar no lugar de outras pessoas, quando
se é programador você deixa de ser consumidor e passa a ser um apreciador. Toda
aplicação que você executa, você passa a olhar com outros olhos e pode até se
perguntar: "Mas como o desenvolvedor fez isso?", você deixa de ser tão exigente
quanto a funcionalidade do software pois sabe que isso dá muito trabalho de se
programar e quando você entra em contato com o desenvolvedor para resolver algum
bug, você já sabe que aquele desenvolvedor também é um ser humano e não é seu
escravo. Produzir software te torna mais colaborativo, você aprende a lidar com
pessoas e isso te torna uma pessoa melhor.

O que eu quero dizer com "seja um produtor de software", é, pare de ficar
procurando alternativas, pare de esquentar a cabeça com coisas superficiais como
a cor de uma interface e busque desenvolver um software você mesmo! Quando você
tentar produzir algo novo vai perceber que as opções existentes nem eram tão
ruins assim e no fim ao invés de reinventar a roda, você vai procurar melhorar
algo já existente.

Desenvolver um software não deve ser um bicho de sete cabeças, deve ser um
hobby, uma paixão se você é produtor de software **parabéns!** Bem-vindo(a) ao
1% da população mundial.
