+++
title = "Servidor web simples sem instalar nada"
date = "2020-06-24"
author = "Cézar Campos"
description = "Como ativar um servidor web que já vem embutido em todas as distribuições Linux sem precisar instalar nada, bom para webdevs e etc."
tags = ["Dica vivaolinux"]
+++

Essa dica é para aqueles _web designers_ que querem testar o próprio site antes
de ir pro ar, mas não querem instalar o Apache ou o Nginx para não ocupar muito
espaço no computador.

O _Busybox_ já vem instalado em muitas distribuições e com ele um servidor web
simples. Essa dica visa facilitar o dia a dia de programadores e web designers
que lidam com diversos sites ao mesmo tempo e querem testar as suas soluções
antes de ir para o ar.

Para instalar o Busybox no _Debian_:

```console
# apt install busybox
```

Para iniciar o servidor no diretório atual:

```console
$ busybox httpd -p 127.0.0.1:8080 -h $PWD
```

Para derrubar o servidor:

```console
$ pkill busybox
```

Simples assim! E se você quiser pode criar duas funções e colocá-las no fim do
seu _.bashrc_ para poder iniciar e parar o servidor com um comando mais fácil:

```bash
function start_web_server(){
    busybox httpd -p 127.0.0.1:8080 -h $PWD
}

function stop_web_server(){
    pkill busybox
}
```

E então basta entrar no repositório do seu site e digitar:

```console
$ start_web_server
```

e para terminar:

```console
$ stop_web_server
```

É isso.
