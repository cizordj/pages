+++
title = "Qual é o sentido da vida"
date = "2022-09-28"
author = "Cézar Campos"
description = "Nesse artigo eu abordo o meu ponto de vista sobre a pergunta: Qual é o sentido da vida?"
tags = ["Estilo de Vida"]
+++

O sentido da vida é a **morte**, porém antes de nos aprofundarmos no significado
desta declaração vamos entender primeiro o que significa a palavra “sentido”.

Na física a palavra “sentido” significa “destino”, “orientação” e perguntar
“Qual é o sentido da vida” é o mesmo que dizer “Qual é o destino da vida”, como
todos sabem a vida termina em morte, portanto o sentido das nossas vidas é a
morte.

No entanto esta é uma resposta bastante sem graça e chata porque não é isso que
se espera quando alguém pergunta qual é o sentido da vida. Isso acontece porque
estamos fazendo a pergunta errada e o que diferencia uma vida da outra é a
direção. Todas as vidas terminam da mesma forma, o que difere uma da outra são
os rumos que elas tomaram, dependendo a escolha o caminho será mais longo e
também mais árduo.

Falando sério agora, o real motivo da pergunta “qual é o sentido da vida” é
descobrir por qual razão vida existe, qual é o seu objetivo, o porquê de
estarmos aqui. Para mim estamos aqui para melhorar, então o “sentido” da vida é
a evolução. Isso faz sentido tanto na biologia quanto no espiritualismo, na
biologia os seres que se adaptam melhor sobrevivem, isto é resultado de uma
evolução enquanto que na espiritualidade nós estamos aqui para pagar pelos
nossos pecados e tentar alcançar a virtude.

Portanto quando alguém lhe perguntar “Qual é o sentido da vida” basta responder:
_A vida possui o sentido que a gente dá para ela_, ou se você estiver um pouco
mais sorumbático responda com a palavra “morte”.
