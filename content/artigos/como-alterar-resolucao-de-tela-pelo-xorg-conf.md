---
title: "Como alterar a resolução de tela pelo Xorg.conf"
date: "2020-05-14"
author: "Cézar Campos"
description: "Aqui eu abordo como se faz para mudar a resolução de tela no Linux através do arquivo de configuração do Xorg, tudo feito na unha manualmente"
tags: ["Dica vivaolinux"]
language: "pt-br"
---

Recentemente eu tive que mudar o _driver de vídeo_ no meu
_Debian_ e encontrei um problema que muitos aqui no VOL se
depararam antes. A situação em que a resolução do monitor está errada e
sua última saída foi mexer no _xorg.conf_ para fixar a resolução
correta.

Sim eu estou ciente dessa dica:
[Mudando resolução de tela pelo xorg.conf](https://www.vivaolinux.com.br/dica/Mudando-resolucao-de-tela-pelo-xorg.conf)

Porém o método que vou lhes apresentar é um pouquinho diferente.
Crie um novo arquivo de configuração (com o Xorg desligado):
```console
# X -configure
```
```
. . .
Xorg detected your mouse at device /dev/input/mice.
Please check your config if the mouse is still not
operational, as by default Xorg tries to autodetect
the protocol.

Your xorg.conf file is /root/xorg.conf.new

To test the server, run 'X -config /root/xorg.conf.new'
```

E agora movemos o arquivo para o diretório correto:

```console
# mv /root/xorg.conf.new /etc/X11/xorg.conf
```

Abra o arquivo com o seu editor de texto favorito:
```console
# editor /etc/X11/xorg.conf
```

Procure pela sessão "monitor" do arquivo, que é mais ou menos assim:

```
Section "Monitor"
Identifier   "Monitor0"
VendorName   "Monitor Vendor"
ModelName    "Monitor Model"
EndSection

```

Como você pode ver, não está especificado nenhuma resolução para este
monitor, então vamos fazer isso. Use o utilitário gtf para gerar a linha
de configuração que você vai colocar no xorg.conf, a sintaxe é a
seguinte:

```console
$ gtf <largura> <altura> <frames por segundo>
```
No meu caso:

```console
# gtf 1280 720 60
```
```
# 1280x720 @ 60.00 Hz (GTF) hsync: 44.76 kHz; pclk: 74.48 MHz
Modeline "1280x720_60.00"  74.48  1280 1336 1472 1664  720 721 724 746  -HSync +Vsync
```

Copie a linha Modeline para dentro da sessão "Monitor" do Xorg.conf:

```
Section "Monitor"
Identifier   "Monitor0"
VendorName   "Monitor Vendor"
ModelName    "Monitor Model"
Modeline     "1280x720_60.00"  74.48  1280 1336 1472 1664  720 721 724 746  -HSync +Vsyn
EndSection
```

E agora para terminar adicione a opção "preferredMode" embaixo do
Modeline:

```
Section "Monitor"
Identifier   "Monitor0"
VendorName   "Monitor Vendor"
ModelName    "Monitor Model"
Modeline     "1280x720_60.00"  74.48  1280 1336 1472 1664  720 721 724 746  -HSync +Vsyn
Option       "PreferredMode" "1280x720_60.00"
EndSection
```

Desse jeito o Xorg vai sempre usar esta resolução que escolhemos para
este monitor em específico, não precisando alterar o modo de outros
monitores.

É isso.
