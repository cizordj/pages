+++
title = "Como modificar a tela do bootloader do Android"
date = "2020-11-06"
author = "Cézar Campos"
description = "Como modificar aquela primeira imagem que aparece quando você liga o telefone."
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado no
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/Como-Modificar-a-Tela-do-Bootloader-do-Android/)

Requisitos:

- Ter o
  [bootloader desbloqueado](/artigos/como-desbloquear-o-bootloader-em-celulares-samsung).
- Saber usar o [Heimdall](/artigos/como-usar-o-heimdall).
- A partição VBMETA do celular vazia.

NOTA: você perderá a garantia se modificar o firmware do aparelho.

## Introdução rápida

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura1.png" alt="Primeira mensagem do bootloader. Fonte: O autor (2020)" position="center" caption="Figura 1 - Primeira mensagem do bootloader" captionPosition="center" >}}

Nos celulares da Samsung, quando você desbloqueia o bootloader e depois instala
alguma custom ROM, a tela do bootloader fica poluída com mensagens em vermelho
dizendo que o celular está comprometido e que não é recomendável colocar nada de
sensível no smartphone. Isso serve para alertar que o software não é original e
também assustar os novatos para fazê-los acreditar que o smartphone ficará
estragado para sempre.

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura2.png" alt="Segunda mensagem do bootloader. Fonte: O autor (2020)" position="center" caption="Figura 2 - Segunda mensagem do bootloader" captionPosition="center" >}}

Isso acontece porque o bootloader foi desbloqueado e o sistema foi modificado de
algum jeito, seja com uma custom ROM ou root. Quando você vai instalar uma
custom ROM, a partição VBMETA é a primeira partição que você vai querer mexer,
pois ela impede que você inicialize o sistema, caso ele não seja original da
fabricante.

## Como esvaziar o VBMETA

Caso você tenha feito root usando o método
[Magisk](/artigos/como-fazer-root-em-celulares-samsung) você já tem o VBMETA em
branco no seu celular, mas caso queira mudar a tela do bootloader sem fazer
root, você ainda pode fazê-lo extraindo o arquivo "vbmeta.img" proveniente
daquele tutorial, extraia o arquivo de dentro do magisk_patched.tar e instale
usando o Heimdall na partição VBMETA.

Se você usar o VBMETA original do aparelho, poderá dar de cara com o seguinte
erro:

    Only official binaries are allowed to be flashed.

E no fim, será obrigado a instalar toda a stock ROM por completo de novo.

## Como vamos fazer

A partição responsável por essa mensagem na tela de inicialização é a PARAM,
quando você baixa a stock ROM do site da Samsung, o arquivo que vai nessa
partição é o "param.bin".

Se você olhar no terminal, vai perceber que esse arquivo é um mero arquivo
".tar":

```console
$ file param.bin
```

```
param.bin: POSIX tar archive (GNU)
```

Com isso, então podemos extraí-lo em algum lugar.

```console
$ mkdir PARAM
$ tar -xf param.bin -C PARAM/
```

Dentro dessa pasta, você vai ver que existe um monte de arquivos de imagens.

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura3.png" alt="Uma captura de tela de uma pasta aberta cheia de arquivos. Fonte: O autor (2020)" position="center" caption="Figura 3 - Listagem de arquivos da partição" captionPosition="center" >}}

Antes que você saia modificando tudo por aí, vale ressaltar que todos os
arquivos têm permissões diferentes e você deve lembrá-las antes de empacotar o
"param.bin" de novo.

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura4.png" alt="Propriedades do arquivo de imagem. Fonte: O autor (2020)" position="center" caption="Figura 4 - Propriedades do arquivo de imagem" captionPosition="center" >}}

Como por exemplo este arquivo de imagem, no final do processo ele deve voltar a
ser somente-leitura.

## Mão na massa

Você lembra das mensagens na tela de boot?

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura1.png" alt="Primeira mensagem do bootloader. Fonte: O autor (2020)" position="center" caption="Figura 1 - Primeira mensagem do bootloader" captionPosition="center" >}}

Estas mensagens são meros arquivos de imagem e você pode editá-los, procure na
pasta PARAM os mesmos arquivos e abra com algum editor de sua preferência.

No meu caso, eu usei o Inkscape:

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura5.png" alt="Uma tela onde a imagem está sendo digitalmente editada. Fonte: O autor (2020)" position="center" caption="Figura 5 - Tela de edição do Inkscape" captionPosition="center" >}}

Coloquei um quadrado preto em cima da imagem com a mesma resolução e depois
salvei o arquivo como "booting_warning.png":

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura6.png" alt="Uma tela onde a imagem está sendo digitalmente editada. Fonte: O autor (2020)" position="center" caption="Figura 6 - Tela editada no Inkscape" captionPosition="center" >}}

Agora, é só converter o PNG para JPG, substituir o arquivo original com esse que
você modificou e 'Voilà'. Ao invés da mensagem de aviso, você só vai ter um
quadrado preto invisível na tela inicial.

Não se esqueça de alterar o modo do arquivo para o mesmo que tava antes, no meu
caso era o 0444.

```console
$ chmod 0444 boot_warning.jpg
```

Alguns modos podem variar dependendo o arquivo.

O próximo arquivo que vamos editar é o "svb_orange.jpg", ele é o responsável
pela segunda mensagem na tela de boot:

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura2.png" alt="Segunda mensagem do bootloader. Fonte: O autor (2020)" position="center" caption="Figura 2 - Segunda mensagem do bootloader" captionPosition="center" >}}

O que vamos fazer, simplesmente, é apagar ele e substituir com outra tela
normal, nesse caso o "logo.jpg":

```console
$ rm svb_orange.jpg
rm: remover arquivo comum 'svb_orange.jpg' protegido contra escrita? Sim
$ cp logo.jpg svb_orange.jpg
```

Edite todas as imagens que achar necessário e deixe-as limpas, com nenhuma
mensagem de aviso. Após todas as edições junte todos os arquivos dessa pasta em
um novo arquivo ".tar".

```console
$ tar -cf param_modificado.bin *
```

Este novo arquivo "param_modificado.bin" será instalado na partição PARAM do seu
celular. Coloque o celular no modo download e conecte-o ao computador.

Use o seguinte o comando do Heimdall para instalar o arquivo na partição certa.

```console
$ sudo heimdall flash --pit A20_SWA_OPEN.pit --PARAM param_modificado.bin
```

```
. . .
Uploading PARAM
100%
PARAM upload successful

Ending session...
Rebooting device...
Releasing device interface...
. . .
```

Depois disso, o celular vai reiniciar já com a nova tela, confira os resultados
do meu:

{{< figure src="/artigos/como-modificar-a-tela-do-bootloader-do-android/figura7.png" alt="Celular da Samsung reiniciando. Fonte: O autor (2020)" position="center" caption="Figura 7 - Bootloader com a imagem nova" captionPosition="center" >}}

É isso, lembrando que você pode desenhar qualquer coisa naquelas imagens, não
precisa deixar a logo da Samsung necessariamente.

Testado no Galaxy A20 (2019).

Over 'n out.
