+++
title = "Navegadores webkit vs placas Nvidia"
date = "2021-04-18"
author = "Cézar Campos"
description = "Aqui eu explico por que os navegadores baseados em Webkit não funcionam com as placas de vídeo da Nvidia"
tags = ["Dica vivaolinux"]
+++

A _NVidia_ nunca foi fã de navegadores baseados em
_WebKit_, como o Gnome Web e o Surf browser. Se você tem uma
placa de vídeo e usa os drivers proprietários, é bem provável que você
não consiga usar esses navegadores.

{{< figure alt="Captura de tela de uma mensagem de erro que o surf browser mostra ao ser aberto via terminal com o site do viva o linux. Fonte: O autor (2021)" caption="Figura 1 - Surf browser falhando ao abrir vivaolinux.com.br" captionPosition="center" position="center" src="/artigos/navegadores-webkit-vs-placas-nvidia/figura1.png">}}

A razão pela qual isso não funciona, é porque o navegador tenta usar a
aceleração via hardware. Para que você consiga usar esse tipo de
navegador com placas da NVidia, é preciso desativar essa aceleração e
para fazer isso basta definir uma variável de ambiente:

```bash
WEBKIT_DISABLE_COMPOSITING_MODE=1
```

Coloque esta variável dentro do arquivo _/etc/environment_ e você
estará pronto para usar!

{{< figure alt="Captura de tela do navegador surf exibindo o site vivaolinux.com.br. Fonte: O autor (2021)" caption="Figura 2 - Navegador surf abrindo vivaolinux.com.br" captionPosition="center" position="center" src="/artigos/navegadores-webkit-vs-placas-nvidia/figura2.png">}}

Basta reiniciar o computador para isso dar certo.
Até a próxima dica. 
😛
