+++
title = "Shall we forsake gran prime"
date = "2019-05-08"
author = "Cézar Campos"
description = "In this little article I tell people that they should abandon a certain Samsung phone"
tags = ["Aleatórios"]
language = "en-us"
+++

    Originally published on Xda-developers' forum

# A bit of history

I don't know if you guys have noticed, but there are a lot of ROMs not being
updated anymore because the devs are migrating to newer phones or whatever. In
this thread I want to show you guys that installing new custom ROMs and looking
for updates don't worth your time.

This phone has been released on October 1st, 2014 and now it's 2019 which means
that we're using a five years old phone (which is not bad). Samsung gave up on
it in 2015 when they launched (the last) lollipop update. But then a lot of
developers started working to make our beloved GP an useful phone with the
latest cool stuff that newer android versions give to us. It was wonderful when
every Tuesday there was a building going on in a Jenkin's server and afterwards
we could download the ROM just to have the latest version of Cyanogenmod (good
old days that never come back). For me, the last stable custom ROMs were the
Nougat and Marshmallow versions. Afterwards some of the devs have changed their
phones and stopped creating new stuff for GP (I'm not blaming anyone for that)
because the phone was getting too problematic to work with.

Even so, some of the devs were still persisting on making new builds and fixing
the bugs but those new ROMs weren't ever going to get out of the **beta** stage.
Even if you see new custom ROMs builds today they are not gonna be finished as
their kernel need to be worked on fixing bugs and improvements. That's why they
all have the same bugs because they use the same base which has these bugs.

# The real last update

The last modification in the kernel has been made on Oct 17th, 2018.

[Last commit](https://github.com/Galaxy-MSM8916/android_kernel_samsung_msm8916/commit/fef3eaa9d80910ac968222331ba006adaa1d7051)

This means that all of these new custom ROMs in 2019 are not getting their bugs
fixed. Please do not ask the devs for updates and improvements because they've
already forsaken this phone long time ago.

We have to be grateful that they have spent a lot of their time and money (I
don't know which is most valuable) programming, looking for bugs and losing
sleep nights just to bring us (users) pieces of software FOR FREE.

For all developers my biggest Thank you !

# My grand prime's retirement

At this time I am using the oldest stock ROM ever, the KitKat. It's very stable
specially when you root it and remove all the bloatware stuff. Its interface is
not that bad, I personally think it's cuter that lollipop's. Of course you will
miss google's smart lock and a lot of apps that are not supported anymore, such
as Swiftkey, Gboard and newer stunning browsers.

I think that we users should let our grand prime to rest in peace and to follow
our own lives. It has been a great adventure to brick this phone a lot of times
and being one or two days without social media because of a software brick.

What do you think of all of this?

Have a great day!

![](/artigos/shall-we-forsake-gran-prime/figura1.png)
![](/artigos/shall-we-forsake-gran-prime/figura1.png)
