+++
title = "O Molotov está no ar"
date = "2021-10-07"
author = "Cézar Campos"
description = "O meu programa Molotov já foi aceito nos repositórios do Debian e também dou mais alguns detalhes de como foi todo esse processo."
tags = ["Dica vivaolinux"]
+++

Originalmente publicado no
[vivaolinux.com.br](https://www.vivaolinux.com.br/dica/O-Molotov-esta-no-ar/)

Bom dia pessoal do Viva o Linux, eu só gostaria de passar para avisar que o
Molotov foi aprovado no dia 18 de agosto de 2021 e agora está disponível nos
repositórios do Debian SID. Para obter o programa agora você pode rodar um
comando arbitrário do apt-get e instalar ele direto dos repositórios.

{{< figure alt="Imagem de um terminal com o resultado do comando apt search molotov. Fonte: O autor (2021)" caption="Figura 1 - Molotov encontrado nos repositórios" captionPosition="center" position="center" src="/artigos/o-molotov-esta-no-ar/figura1.png" >}}

Para quem não sabe o que é o Molotov aqui está um
[artigo](/artigos/apresentando-o-molotov/) que eu fiz de apresentação.

E para quem pediu, o nosso amigo
[@mauricio123](https://www.vivaolinux.com.br/~mauricio123) criou uma build desse
programa no Slackware e ela está disponível em seu GitHub pelo seguinte
[link](https://github.com/mxnt10/Slackware15-Reposity/blob/master/Others/molotov.SlackBuild).

Também quero aproveitar para compartilhar com vocês algumas coisas que
aconteceram nos últimos meses que pode servir de aprendizado, tanto para quem
quer programar quanto para aqueles que desejam um dia se aventurar no mundo de
pacotes do Debian.

O Molotov apesar de ser um software livre licenciado sob a GPL não foi aceito na
seção principal do Debian, para poder utilizá-lo você deve habilitar o
repositório contrib na sua distribuição Linux. Essa foi uma recomendação dada
pelo meu [padrinho](https://wiki.debian.org/SponsoredMaintainer) e por um
webmaster porque o Molotov depende de uma imagem do Windows para poder funcionar
e como o Windows é um software proprietário então o Molotov não pode ir para a
seção principal do Debian. Eu aceitei porque isso faz muito sentido, talvez se o
Molotov tivesse alguma outra funcionalidade que não dependesse do Windows então
talvez ele seria aceito na ramificação principal.

Outro detalhe que eu gostaria de expor é que como esse pacote nunca esteve no
Debian, ele esteve na fila para a revisão por vários meses e teve de ser
auditado por um ftpmaster antes de subir. Em outras palavras, esse é o motivo
pela qual o Debian sempre tem softwares "desatualizados" pois o Molotov já teve
vários bugs corrigidos desde quando eu criei o pacote pela primeira vez e a
primeira versão só subiu até agora. Outro ponto a ser mostrado é que agora o
pacote está na distribuição SID, se não houver nenhum bug crítico durante vários
dias o pacote então vai ser passado para o Testing.

É isso, espero que vocês tenham uma boa semana e façam bom uso do Molotov.
