+++
title = "Softwares que eu uso e recomendo"
date = "2022-02-05"
author = "Cézar Campos"
description = "Após vários pedidos eu estou colocando aqui os softwares que eu uso no meu dia-a-dia."
tags = ["Estilo de Vida", "Tecnologia"]
+++

Após vários pedidos eu estou colocando aqui os softwares que eu uso no dia a
dia. Só vou colocar aqui programas que eu usei de verdade por bastante tempo.

## Prioridades

Para mim o programa deve ser gratuito, além disso não deve me obrigar a criar
conta em nenhum lugar na web afim de usá-lo. Se o programa puder ser usado
offline é melhor ainda! Softwares que requerem a minha identificação para ser
usados são nojentos, então eu evito contato com eles.

Tenho preferência por programas com uma bela interface gráfica e que respeitam a
minha privacidade acima de tudo (isto é, sem telemetria).

## Sistema operacional

Eu estive usando o Debian _testing_ como meu principal sistema operativo desde
2017 e a principal razão pela qual eu uso o Debian é que ele **resolve os meus
problemas!** Para mim como desenvolvedor o Debian é bom porque ele possui várias
ferramentas de desenvolvedor já compiladas e configuradas, então a única coisa
que preciso fazer é rodar um `apt-get install` e o problema está resolvido.

## Terminal

Por muito tempo eu estive usando o meu próprio terminal
[suckless term](https://github.com/cizordj/stterm) porém devido a aos trabalhos
do dia a dia eu percebi que esse terminal possui algumas falhas, porém esse é o
terminal mais perfeito que eu já usei até hoje, principalmente para se usar com
o _vim_. Em termos técnicos e de aparência esse terminal é o mais bem feito que
eu já vi, porém ele não suporta emojis e por causa disso eu não consigo usá-lo
meu dia a dia. Atualmente estou usando o kitty que possui alguns comportamentos
parecidos e suporta emojis.

## Gestor de janelas/Interface gráfica

O gerenciador de janelas que uso atualmente é o [i3](https://i3wm.org/) junto
com a minha própria configuração do
[i3-themer](https://github.com/cizordj/i3-themer). Eu gosto de i3 porque foi o
meu primeiro _wm_ e até agora não encontrei razões para sair dele. Ele me dá um
fluxo de trabalho muito interessante e consigo ter controle absoluto do sistema
com ele. Além disso o i3 é tão leve que pode rodar em um computador
[com 17 anos de idade!](/artigos/i3-para-computadores-antigos)

Agora no que tange às interfaces gráficas do Linux a única que eu gosto e uso é
o Gnome, não tem muito o que falar, o Gnome é a interface que oferece o visual
mais consistente, minimalista e que não cansa os olhos. Além disso o Gnome
oferece muitas facilidades que você jamais teria em um sistema Unix de verdade.
O fluxo de trabalho do Gnome é incrivelmente simples e deixa você focado logo de
cara, se você entrar fundo na forma na qual ele foi desenhado você vai perceber
que a falta de customização nem é uma coisa tão ruim assim! É claro, você ainda
pode mudar coisas como o tema de ícones e o tema da interface e é o que eu
sempre faço.

## Editor de texto/Programação

Desde 2019 eu venho usando o VIM como meu principal editor de texto e a razão
pela qual eu comecei a usar o VIM foi porque na época eu tinha um computador
muito fraco e ele não era capaz de rodar as principais IDEs do mercado, eu já
tentei usar o InteliJ, o VScode, o Netbeans e o PHPstorm, o problema de todos
esses editores é que eles são pesados demais e não rodam em computadores
antigos. O Netbeans era feito em java e toda vez que eu abria ocupava quase 1Gb
da memória RAM, mesmo sem fazer nada, o VScode então nem se fala, por ser feito
em Electrum essa IDE é inchada por natureza.

O VIM é absurdamente leve e poderoso, a curva de aprendizado é tão grande que
usar o VIM como editor é quase como um estilo de vida.

## Navegador

Eu uso o Epiphany tanto para navegar na internet quanto para desenvolver sites.
O Epiphany tem tudo que você pode querer em um navegador, um bloqueador de
anúncios embutido e privacidade de verdade. O melhor de tudo: Ele é minimalista!

O problema do Epiphany é que eu não consigo usá-lo no meu computador graças aos
drivers da Nvidia, a Nvidia é uma fabricante de placa de vídeos que não dá a
mínima para os sistemas Unix e os seus drivers não funcionam com nada que usa o
Webkit como motor. Além do Epiphany o navegador
[Surf](https://surf.suckless.org/) também não funciona na minha máquina.

Atualmente o navegador que uso no dia a dia é o bom e velho Firefox, ele é o meu
porto seguro, tanto para navegar quanto para desenvolver para a web.

## Utilidades

- Tocador de música:

Eu uso o _cmus_ como meu principal tocador de música.

- Cliente de torrent:

Até hoje o único cliente de torrent que nunca me deixou na mão foi o
[Web torrent desktop](https://webtorrent.io/). Ele é multiplataforma e mil vezes
mais confiável que o clássico uTorrent.

- Cliente de e-mail:

O melhor cliente de e-mail que já usei é o Geary, ele se integra muito bem ao
Gnome e tem uma interface absurdamente fácil de usar.

## Produtividade

### Edição de áudio

Eu uso o **Audacity** como principal ferramenta de edição de áudio, eu inclusive
já produzi um CD com ele!

### Edição de vídeo

Eu raramente edito vídeos, mas quando preciso editar o único editor que atendeu
as minhas necessidades até hoje foi o [Pitivi](http://www.pitivi.org/?go=tour),
por ser um software livre o Pitivi não deixa marcas d'água em seus vídeos e
entrega tudo que há do bom e do melhor de graça sem precisar pagar para ter os
recursos PRO. Isso sem falar que o Pitivi se integra com maestria ao ambiente
Gnome.

### Edição de imagem

Eu quase nunca edito imagens, porém quando eu preciso eu uso o
[Inkscape](https://inkscape.org). Eu sei, o Inkscape não é um editor de imagens
e sim um editor de gráficos vetoriais, mas foda-se eu faço o que eu quiser com o
software.

### Edição de documentos

Felizmente para Linux nós temos um editor de documentos que está a altura do
Microsoft Office, e não, não é o LibreOffice (desculpa). O software a qual me
refiro é o [OnlyOffice](https://www.onlyoffice.com/pt/). Ele tem uma interface
que lembra bastante o Office da Microsoft e o melhor de tudo é que ele é
software livre!

No passado eu cheguei a usar o WPS Office como a minha suíte padrão, ele tem uma
interface extremamente agradável e fácil de usar, mas o problema desta suíte é
que ela é de código fechado e é Chinesa. Então você já sabe né. 😉

## Hardware que eu uso

### Computador

Eu uso um computador com a placa mãe da Gigabyte
[GA-Q35M-S2](https://www.gigabyte.com/Motherboard/GA-Q35M-S2-rev-1x#ov)
fabricado em 2007. Ele possui 8 Gb de memória RAM em quatro canais e um
processador Quad core Xeon, bastante potente até para os dias de hoje. Essa
placa mãe possui uma BIOS monstruosa que faz o computador demorar para ligar,
mas isso não é problema para mim.

#### Placa de vídeo

Uso uma placa da Nvidia modelo GTX 750 Ti. Esta é uma placa razoável para os
dias de hoje e consegue até rodar GTA 5. Para mim o mais importante é que a
placa tenha o máximo tempo de suporte possível, isto é, que os drivers sejam
compatíveis com o Linux por bastante tempo, não ligo pra quanta memória de vídeo
uma placa tem desde que ela funcione com a interface gráfica que estou usando.
No futuro eu pretendo mudar para as placas de vídeo da Intel se ela algum dia
decidir lançar.

#### Mouse

Eu tenho um mouse sem fio da marca Easterntimes Tech, modelo X-08. Super
recomendo comprar, é barato, gamer e grande. O único defeito deste mouse é que
ele consome muita pilha.
