---
title: 'Problemas para instalar WoeUSB no Debian, Ubuntu e derivados'
date: '2020-05-19'
author: 'Cézar Campos'
description:
  'O WoeUSB está com problemas de dependências ultimamente o que impede as
  pessoas de usar a última versão, aqui nessa dica eu trago a solução.'
tags: ['Dica vivaolinux']
language: 'pt-br'
---

Ultimamente eu vi algumas pessoas relatando aqui no fórum sobre problemas com a
instalação do WoeUSB. Essa dica visa esclarecer o que está acontecendo e como
resolver este problema.

O WoeUSB está com problemas de empacotamento, principalmente nas distros Debian.
Ele depende de um pacote chamado `libwxgtk3.0-dev` o qual não está mais
disponível no repositório e isso faz com que ele não seja possível de instalar
no Debian e Ubuntu, nem mesmo a versão do PPA do Ubuntu está funcionando. Isso
acontece porque o pacote mudou de nome, logo o mantenedor do pacote deveria
atualizar o sistema de empacotamento para a dependência com o novo nome
`libwxgtk3.0-gtk3-dev`.

Como resolver este problema? Teoricamente qualquer pessoa pode baixar o
código-fonte do WoeUSB, atualizar as dependências no arquivo
_WoeUSB/debian/control_ e fazer um novo pacote .deb.

Mas como essa dica é voltada a iniciantes, então eu vou passar o link de alguém
que já fez isso.

Acesse esse link:
[WoeUSB doesn't install on Ubuntu 20.04 - Issue #311 - slacka/WoeUSB - GitHub](https://github.com/slacka/WoeUSB/issues/311#issuecomment-628021588)

{{< figure src="/artigos/problemas-para-instalar-woeusb-no-debian-ubuntu-e-derivados/figura1.png" alt="Captura de tela de um comentário no github dando instruções de como corrigir o problema do WoeUSB. Fonte: O autor (2020)" caption="Figura 1" position="center" captionPosition="center">}}

Clique em `woeusb.zip` e quando terminar de baixar extraia o arquivo.

Você vai ver um pacote deb chamado: **woeusb_3.3.1_amd64.deb**

Instale-o via interface gráfica com o _Gdebi_ ou pelo terminal:

```console
# apt install ./woeusb_3.3.1_amd64.deb
```

O apt deverá resolver as dependências para você e instalar o pacote.

{{< figure src="/artigos/problemas-para-instalar-woeusb-no-debian-ubuntu-e-derivados/figura2.png" alt="Captura de tela da interface gráfica do WoeUSB funcionando perfeitamente. Fonte: O autor (2020)" caption="Figura 2" position="center" captionPosition="center">}}

É isso, aproveite o WoeUSB.
