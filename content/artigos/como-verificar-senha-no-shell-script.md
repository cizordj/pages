+++
title = "Como verificar senha no shell script"
date = "2019-01-29"
author = "Cézar Campos"
description = "Tutorial para completos iniciantes sobre como se faz para criptografar senhas e verificar se elas estão corretas em shell script."
tags = ["Dica vivaolinux"]
language = "pt-br"
+++

Olá tudo bem? Se por alguma razão bem aleatória você quer pôr uma senha no seu
shell script para dar acesso a determinada função, ou qualquer outra coisa (só
depende de você), dá para verificar se ela está correta sem colocar a sua senha
de verdade no código-fonte.

## Método errado

Aqui vou testar a autenticidade que, por incrível que pareça é bastante comum de
se ver por aí.

```bash
#!bin/sh
echo Digite a sua senha...
read senha
if [ $senha -eq "102374" ] ; then
  echo "Senha correta"
else
  echo "Senha ERRADA!"
fi
```

Por que este método é errado? Colocar a senha em texto puro no script é uma
tremenda falha de segurança, qualquer espertinho pode dar um grep no seu script
e localizá-la. Mas como verificar a senha sem expô-la no shell script? Usando
uma soma de verificação.

Sinta-se livre para testar o código abaixo e digite a mesma senha que estava
exposta no código acima.

```bash
#!/bin/sh
echo Digite a sua senha...
read senha
echo "dbbe235510a12c4ceea53cfa2150859e  senha" > senha.md5
echo $senha > senha
md5sum -c --status senha.md5
if [ $? -eq 0 ] ; then
  echo "Senha correta"
else
  echo "Senha ERRADA!"
fi
rm senha.md5 senha
```

Viu como funciona? Foi criado uma forma de verificação da senha ao invés de
expor no código fonte o seu conteúdo cru. Bem mais seguro, sem falar que você
pode usar verificações ainda mais pesadas como _SHA256_ ou _SHA512_.

## Criando a soma de verificação

Para criar a soma de verificação da sua senha é simples. Faça no terminal:

```console
$ echo sua_senha > arquivoqualquer
```

e depois:

```console
$ md5sum arquivoqualquer
```

que vai sair com a soma de verificação da sua senha para você pô-la no
código-fonte.
