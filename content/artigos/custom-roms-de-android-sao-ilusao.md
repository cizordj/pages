+++
title = "Custom ROMs de Android são ilusão"
date = "2022-10-04"
author = "Cézar Campos"
description = "Não perca o seu tempo instalando custom ROMs de android, isso vale o aprendizado, mas não vale o seu tempo!"
tags = ["Blackpill", "Tecnologia"]
+++

Não perca o seu tempo aprendendo a customizar o firmware do seu celular, seja
para jogo ou para obter as últimas “novidades” do Android. Após 8 anos
compilando, quebrando e modificando ROMs de Android eu cheguei a conclusão de
que tudo isso é uma grande perda de tempo, não somente **eu** perdi o meu tempo
como também o pessoal dos fóruns online adora fazer os outros disperdiçarem seu
tempo também. Eu cheguei até a excluir minha conta no
[XDA developers](https://forum.xda-developers.com/) devido ao tamanho desgosto
que tenho por essa comunidade, ninguém sabe o que está fazendo, parece que
ninguém se importa com o que realmente interessa!

O aparelho celular contém informações demais sobre as pessoas e chegou ao ponto
de **substituir** o cartão de crédito e os documentos oficiais impressos, isto
somado a um sistema operacional _mal feito_ e cheio de lixo que as fabricantes
produzem. Isso é uma bomba relógio que está prestes a explodir, não há
privacidade e nem segurança de verdade. Todo programador **só faz bosta** e as
grandes corporações não estão nenhum pouco interessadas na forma como o sistema
é implementado. **Elas só querem os seus dados**, não importa se o seu CPF é
usado como autenticação no login do seu banco.

## O que interessa na hora de customizar o Android?

O que eu faço nos celulares que eu mexo são as seguintes coisas:

- Se livrar da obsolência programada.
- Remover aplicativos inúteis (bloatware)
- Remover aplicativos espiões (spyware)
- Remover propagandas
- Remover a obrigatoriedade de criar contas online

Tudo isso faz com que qualquer aparelho celular seja confiável e útil para o
dia-a-dia, isso faz com que a maioria dos aparelhos que são “obsoletos” voltem a
operar, isso porque na maioria dos casos o aparelho em si não é obsoleto e sim o
seu fabricante o torna através de “atualizações de segurança e desempenho”. A
obsolência programada é um meio que as fabricantes de aparelho celular
encontraram para fazer com que o consumidor compre um celular novo de tempos em
tempos, você pode notar que o _hardware_ é quase sempre o mesmo, só muda a
quantidade de pixels que há na câmera e o que realmente deveria mudar elas não
mudam, que é a quantidade de memória interna e núcleos do processador. Com uma
quantidade maior de memória interna é possível manter o sistema Android
atualizado por mais tempo já que a cada lançamento o sistema fica mais grande e
tornar o celular mais resistente à obsolência de _hardware_, porém o que chama
atenção do consumidor é o _marketing_ e a quantidade de câmeras atrás da tela.

O problema com as contas online é que você, usuário final, fica a mercê dos
desejos da fabricante, você pode até perder o seu celular para sempre caso você
esqueça a sua senha, se você por um acaso trocar de número de telefone existe o
risco de você perder o acesso à sua conta por não poder alterar o número (ou a
senha em si), mesmo que você formate o telefone não é possível desbloquear o
aparelho por questões de “segurança” e mesmo que você instale uma ROM
customizada o aparelho não permite porque possui o _bootloader_ bloqueado, como
é o caso dos _Xiaomis_. Eu até entendo que isso é para prevenir furtos do
aparelho e roubos de dados posteriores, porém a raiz do problema não é esse, o
problema está em guardar dados pessoais dentro do aparelho. Além disso, a ideia
por trás de você criar uma conta é “aproveitar” todos os “benefícios” que a
fabricante do aparelho tem para te oferecer, no entanto esses benefícios se
resumem a coleção de dados para fins de _publicidade_ e sabe lá Deus para que
tanta política de privacidade... É como eu digo:

    Se há política, não há privacidade

Se os celulares não guardassem tantas informações sensíveis sobre a gente, não
haveria a necessidade das fabricantes colocarem mecanismos de defesa, graças a
isso hoje nós temos aparelhos que nos escravizam e espionam 24 horas por dia.
Literalmente o _sonho de Stalin_.

## O que há de errado com as pessoas do XDA-developers

Por experiência própria as pessoas de cada fórum que frequentei eram do mesmo
tipo: **Um bando de zé roela que só pensa em joguinho**. A maioria dos
“desenvolvedores” que tive o **desprazer** de conhecer só sabiam seguir
tutoriais de _YouTubers_ indianos e mudar o governador da CPU para fazer com que
o sistema parecesse mais rápido, mudando a quantidade de memória alocada para a
GPU como se o sistema operacional não precisasse de RAM para operar o resto, uma
idiotisse atrás da outra. Eu cansei de instalar ROMs no meu celular que
prometiam ser mais enxutas e rápidas e não poder usar no dia-a-dia porque algum
**idiota** achou que era boa ideia remover o framework de pagamentos da Samsung,
o que bloqueou meu aparelho logo em seguida quando eu acessei a minha conta
bancária!

Aprenda uma coisa, se o Android que você está mexendo é todo cagado igual ao da
Samsung, não mexa no framework de pagamentos! Não mexa no Knox! e não toque na
partição **VBMETA**! Essas pessoas não possuem o mínimo de conhecimento sobre
segurança e privacidade, tudo o que elas querem é otimizar o celular para jogar
_Free Fire_ e deixar a tela mais bonita com cores personalizadas.

Outra coisa que eu acho absurda é a quantidade de ROMs que o mesmo cara suporta,
isso vale tanto para distribuições Android quanto para distribuições de sistemas
de recuperação como o [TWRP](https://twrp.me/ 'TeamWin Recovery Project'),
[PitchBlack](https://sourceforge.net/projects/pbrp/ 'Pitch Black recovery') e
[Skyhawk](https://shrp.github.io/#/ 'Skyhwak'). Entenda uma coisa, isso tudo
**não vale de nada!** Isso só é possível porque todas essas ROMs possuem a mesma
base, logo elas são fáceis de compilar, quando uma delas possui uma falha pode
ter certeza de que todas as outras vão possuir a mesma falha e ninguém vai
corrigir. Você pode até reportar ao “desenvolvedor” que criou o tópico do fórum,
porém você perderá seu tempo e ele não vai ter a expertise para corrigir.

Desenvolvimento de ROMs Android é um negócio sério e não deve ser levado na
brincadeira! Eu já acho errado quando o publicante da ROM cria um site todo
bonitinho e entrega uma ROM toda cheia de falhas e erros, isso mostra aonde que
ele passou a maior parte do tempo concentrando os seus esforços.

{{< figure src="/artigos/custom-roms-de-android-sao-ilusao/suported_roms_on_galaxy_a20.png" caption="Figura 1- Lista de ROMs Android suportadas para o Galaxy A20" alt="Fonte: Disponível em <https://eurekadevelopment.github.io/rom11> (acesso em 2022)" >}}

Pode apostar que a experiência vai ser ruim em todas as ROMs apresentadas,
nenhuma delas vai funcionar direito e todas vão se parecer com o Android “puro”,
ou seja, não há nenhuma diferença, é perda de tempo total aprender comandos e
testar as ROMs uma por uma para ver se você encontra aquela distribuição
“ideal”, isso parece muito com _distro-hopping_ não é mesmo? Pois é, a diferença
é que existe o risco de você ficar sem o seu celular e perder a comunicação com
pessoas importantes da sua vida como as do trabalho e familiares.

Leia o artigo
[shall we forsake gran prime](/artigos/shall-we-forsake-gran-prime/) onde eu
alerto as pessoas dizendo que é perda de tempo insistir no gran prime e instalar
novas atualizações de Android, pois não havia nenhum desenvolvedor qualificado
para manter o suporte do aparelho. Aqui está o link para o
[artigo original](https://forum.xda-developers.com/t/shall-we-forsake-grand-prime.3928259/)
caso você queira ler as respostas das outras pessoas.
