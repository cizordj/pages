+++
title = "Como verificar a integridade das stock ROMs da Samsung"
date = "2020-10-28"
author = "Cézar Campos"
description = "Esta dica é para você que baixa stock ROMs para o seu celular da Samsung e gostaria de ver se eles estão inteiros antes de instalá-los em seu telefone"
tags = ["Dica vivaolinux"]
language = "pt-br"
+++

Essa dica é pra você que baixa _stock ROMs_ para o seu celular da _Samsung_ e
gostaria de ver se eles estão inteiros antes de instalá-los no telefone. O
processo é bem simples e pode ser feito em qualquer distribuição Linux.

Hoje de manhã baixei uma nova ROM pro meu celular que foi lançada no começo de
Outubro com novos patches de segurança, a transferência levou mais de seis
horas, então pra garantir que tudo esteja certo eu vou mostrar pra vocês como
vou ver se a stock ROM não foi corrompida durante o download.

O arquivo que baixei é um pequeno zip de uns 3 GB mais ou menos. Depois de
extraído ele liberou novos arquivos que totalizam 4 GB mais ou menos.

```console
$ unzip SM-A205G_1_20200925080504_axdd64ux41_fac.zip
```

```
Archive: SM-A205G_1_20200925080504_axdd64ux41_fac.zip
inflating: BL_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship.tar.md5
inflating: AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar.md5
inflating: CP_A205GNDXS7BTI2_CP16788700_CL19166873_QB34239128_REV00_user_low_ship.tar.md5
inflating: HOME_CSC_OMC_OWO_A205GOWO7BTI2_CL19425033_QB34239415_REV00_user_low_ship.tar.md5
inflating: CSC_OMC_OWO_A205GOWO7BTI2_CL19425033_QB34239415_REV00_user_low_ship.tar.md5
```

Como você pode perceber todos os arquivos terminam com md5, antes de você
instalar no celular você normalmente tira esse md5 do nome e extrai o arquivo
tar, mas na verdade esse md5 está ali pra avisar que a soma de verificação está
no final do arquivo, como você confere isso? Com o comando tail:

```console
$ tail -n 1 AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar.md5
```

```
3d38dbcf7b4c14bb3d78eeec47614284  AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar
```

Agora que sabemos a soma vamos conferir ela com o comando md5sum. Copie e cole
esta saída de texto e salve em um arquivo separado.

```
3d38dbcf7b4c14bb3d78eeec47614284  AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar
```

Renomeie o arquivo que você quer verificar, tirando o md5 no final pra que o
nome fique igual no texto acima, ao mesmo tempo você tem que remover a última
linha do arquivo pois ela não está inclusa na soma de verificação. Por sorte dá
pra fazer essas duas coisas com um único comando:

```console
$ head -n -1 AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar.md5 > AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar
```

Ele vai gerar um novo arquivo sem o md5 no final, o processo pode demorar
dependendo da velocidade do teu HD. Agora use o md5sum pra ver se o arquivo está
íntegro.

```console
$ md5sum -c arquivo_de_texto_que_voce_salvou.txt
```

```
AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar: SUCESSO
```

Ótimo, isso quer dizer que não vou precisar baixar a ROM de novo, faça isso para
todos os arquivos que vieram compactados e você saberá se é seguro atualizar a
ROM do seu celular ou não, agora só falta checar se os outros arquivos estão
inteiros também.

Como o processo é demorado eu fiz um pequeno script que automatiza tudo.

```bash
#!/bin/bash
for file in *.md5
do
    tail -n 1 "$file" >> SUMS
done
for file in *.md5
do
    head -n -1 "$file" > "${file/.md5/}"
done
md5sum -c SUMS
```

Se você ficou curioso, a saída que deu foi essa:

```console
$ ./checar.sh
```

```
AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar: SUCESSO
BL_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship.tar: SUCESSO
CP_A205GNDXS7BTI2_CP16788700_CL19166873_QB34239128_REV00_user_low_ship.tar: SUCESSO
CSC_OMC_OWO_A205GOWO7BTI2_CL19425033_QB34239415_REV00_user_low_ship.tar: SUCESSO
HOME_CSC_OMC_OWO_A205GOWO7BTI2_CL19425033_QB34239415_REV00_user_low_ship.tar: SUCESSO
```

Em caso de erro a saída vai ser essa:

```console
$ ./checar.sh
```

```
AP_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship_meta_OS10.tar: FALHOU
BL_A205GUBS7BTI2_CL19166873_QB34239128_REV00_user_low_ship.tar: FALHOU
CP_A205GNDXS7BTI2_CP16788700_CL19166873_QB34239128_REV00_user_low_ship.tar: FALHOU
CSC_OMC_OWO_A205GOWO7BTI2_CL19425033_QB34239415_REV00_user_low_ship.tar: FALHOU
HOME_CSC_OMC_OWO_A205GOWO7BTI2_CL19425033_QB34239415_REV00_user_low_ship.tar: FALHOU
md5sum: AVISO: 5 somas de verificação calculadas NÃO coincidem
```

É isso, um abraço aí pra todos vocês que mexem com Android.
