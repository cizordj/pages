+++
title = "Como montar arquivos img do Android"
date = "2020-02-24"
lastmod = "2021-02-02"
author = "Cézar Campos"
description = "Aqui eu explico como as imagens do Android são comprimidas e como você pode montá-las dentro do seu ambiente GNU/Linux usando ferramentas open source."
tags = ["Dica vivaolinux"]
language = "pt-br"
+++

Olá, pessoal do VOL.

Enfrentei este problema recentemente, quando precisei extrair um arquivo da
Stock ROM do meu celular, baixei a stock ROM da
[Sammobile](https://www.sammobile.com/) e me deparei com arquivos .img
impossíveis de abrir.

Primeiro, as novas ROMs da Samsung vêm comprimidas pelo algoritmo LZ4, após você
extrair o ".tar", você vai se deparar com um monte desses tipos de arquivos.
Para extraí-los, você precisa instalar o pacote não-livre na distros Debian-like
com o seguinte comando:

```console
$ sudo apt install liblz4-tool
```

E depois, para a descompressão:

```console
$ lz4 -d system.img.lz4
```

Sendo "system.img.lz4" o arquivo que você quer extrair. Após a extração, você
vai se deparar com um arquivo ".img" difícil de montar e que me inspirou a
escrever essa dica.

Ele dava o seguinte erro quando eu tentava montar:

```console
$ mount -o loop system.img /mnt/system
```

```
mount: /mnt/system: tipo de sistema de arquivos incorreto, opção
inválida, superbloco inválido em /dev/loop3, página de código ou
programa auxiliar faltando ou outro erro.
```

Então, consegui resolver esse impasse com a seguinte solução. Se estiver usando
alguma distro Debian-like, instale o pacote "simg2img":

```console
$ sudo apt install simg2img
```

Após isso, digite no terminal:

```console
$ simg2img system.img system.raw
```

Sendo "system.img" o arquivo da ROM que você quer montar. Após a descompressão,
você deve ter um arquivo RAW na sua pasta, então pegue e monte-o normalmente:

```console
$ mount system.raw system/
```

Nesse caso, montei na pasta "system" do meu diretório atual, agora é só abrir
esta pasta e fazer o que você quiser.

Mais detalhes em como reempacotar a imagem do sistema para você instalar no
celular, você encontra neste link:

[Unpack/repack ext4 Android system images « AndWise](https://web.archive.org/web/20141021103926/http://andwise.net/?p=403)

Over and out!
