+++
title = "Introdução ao Heimdall"
cover = "/artigos/introducao-ao-heimdall/figura1.png"
date = "2019-03-10"
author = "Cézar Campos"
description = "Heimdall também é conhecido como o Odin para o Linux, aqui eu apresento os conceitos básicos dele e como fazer para instalá-lo em sua distribuição Linux"
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado em
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/Introducao-ao-Heimdall)

# Uma Alternativa ao Odin para Linux

Entusiastas do mundo Android que gostam de "flashear" customs ROMs em seus
aparelhos, instalar novas recoveries ou até mesmo fazer root, vêem tutoriais no
YouTube ensinando a fazer isso apenas no Odin para Windows, a famosa ferramenta
vazada da Samsung que, além de ser proprietária, não é oficialmente suportada e
mesmo assim é a mais usada pela comunidade.

Estes, quando migram para o Linux não encontram uma alternativa à altura e
muitas vezes se sentem obrigados a recorrer a um PC com Windows para usar o
Odin, eu muitas vezes tive que fazer isso.

Alguns até tentam rodar o Odin pelo Wine, mas que por motivos adversos, o
programa não tem acesso direto à porta USB onde o celular está conectado. Sem
essa funcionalidade, o programa se torna inútil.

Além do Odin, existe uma alternativa chamada "JOdin", que é uma alternativa em
código aberto feita em JAVA e por isso funcionaria em qualquer plataforma, mas
eu tive muita dificuldade em baixar e usar no Linux, e também é limitado a
binários pequenos, ou seja, você não pode instalar Stock ROMs com ele.

Após muita pesquisa, descobri o Heimdall, a internet possui muito pouco conteúdo
sobre ele e por isso os novos Linuxers podem passar tempos dependendo do Odin do
Windows ainda para este tipo de coisa. O Heimdall é livre, open-source e
funciona nativamente no Linux sem precisar de emulação do Windows e nem nada,
dependendo apenas da biblioteca "libusb" para fazer a comunicação com o celular
(nenhum driver da Samsung é necessário).

# O que é Heimdall

![Símbolo do Heimdall](/artigos/introducao-ao-heimdall/figura1.png)

Heimdall é um software multiplataforma feito sob a licença MIT pela Glass
Echidna para instalar firmwares em aparelhos Samsung. Ao contrário do Odin, que
só roda no Windows, o Heimdall pode funcionar em qualquer plataforma, é rápido e
não tem a limitação do Java Odin que mencionei antes.

Ele possui dois binários, um backend chamado Heimdall para os usuários hardcore
e o binário front-end, para os usuários comuns. Ambos fazem a mesma coisa que o
Odin, desde que você saiba manuseá-los.

# É perigoso?

Mexer com o firmware do seu aparelho nem sempre é seguro, você pode causar um
"soft-brick" se instalar o binário errado, pois cada aparelho é único e uma
simples letrinha no modelo faz diferença, além disso, cada modelo possui um
arquivo "pit" que é único para cada variante e usar arquivos pit pegos da
internet, nunca é recomendável.

# Soft-brick

Em uma tradução literal, significa "software quebrado". Quando o seu aparelho
liga, mas o sistema não inicializa por causa de alguma incompatibilidade.

A maioria dos soft-bricks podem ser recuperados ao reinstalar a Stock ROM do
aparelho, então, não se preocupe muito.

# O que dá para fazer no Heimdall?

A princípio, instalar custom recoveries, kernels, stock ROMs e até mesmo extrair
o arquivo pit sem necessitar acesso root no aparelho.

# Arquivo pit, oi?

Arquivo pit (Partition Information Table) é uma tabela de partições única para
cada modelo de aparelho. É sempre recomendável que você use o arquivo que sair
do seu celular para trabalhar com o Heimdall, se o arquivo errado for usado para
o seu aparelho, é soft-brick na certa. Se você usou o Odin a vida inteira,
provavelmente nunca tenha precisado deste arquivo, embora o Odin faça
automaticamente a cópia do pit antes de modificar seu aparelho. Ele é quem dita
o quanto cada partição vai ocupar de memória no seu aparelho e as atualizações
OTA da fabricante podem trazer novas versões do pit, por isso, é sempre
recomendável usar o pit que estiver no seu aparelho. Chega de enrolação e vamos
instalar o Heimdall.

# Como compilar

As distribuições nem sempre trazem a última versão em seus repositórios e por
isso o Heimdall pode não funcionar corretamente sem as correções de bugs. Por
isso, vamos compilá-lo direto do código-fonte.

# Dependências

Vamos começar com as dependências necessárias, como exemplo, usarei a
distribuição Zorin OS que é baseada no Ubuntu Xenial para a compilação:

```console
$ sudo apt install build-essential cmake zlib1g-dev qt5-default libusb-1.0-0-dev git
```

Obs.: a instalação do GIT não é obrigatória, mas facilita o download do
código-fonte.

Pronto. Com as dependências instaladas, baixemos o código-fonte com o git:

```console
$ git clone https://gitlab.com/BenjaminDobell/Heimdall.git
```

Isso criará uma pasta no local chamada "Heimdall", entre nela com:

```console
$ cd Heimdall
```

E agora, começamos a série de comandos para compilar.

```console
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
```

{{< figure src="/artigos/introducao-ao-heimdall/figura2.png" alt="Imagem de um terminal Linux aberto compilando o programa Heimdall" caption="Figura 2 - Compilando o Heimdall pelo terminal">}}

Se nenhum erro aparecer até agora, então você está pronto para compilar. Para
começar digite:

```console
$ make
```

{{< figure src="/artigos/introducao-ao-heimdall/figura3.png" alt="Outra imagem de um terminal Linux aberto compilando o programa Heimdall" caption="Figura 3 - Resultado do comando make">}}

Isso vai levar horas, dependendo do seu hardware, mas quando o processo tiver
terminado, você ainda não terá o Heimdall instalado, apenas os binários dele.
Para instalar o Heimdall de vez, digite o comando final:

```console
$ sudo make install
```

{{< figure src="/artigos/introducao-ao-heimdall/figura4.png" alt="Um terminal Linux aberto mostrando o resultado do comando make install. Fonte: O autor (2019)" caption="Figura 4 - Resultado do make install">}}

Pronto! Agora você tem o Heimdall instalado em seu computador, tanto o frontend
quanto o backend.

Para ter certeza de que ele foi instalado, digite no terminal:

```console
$ heimdall version
```

```
v1.4.2
```

...que deve retornar com a versão que você acabou de compilar.

Para usar o Heimdall com interface gráfica, basta digitar no terminal:

```console
$ sudo heimdall-frontend
```

{{< figure src="/artigos/introducao-ao-heimdall/figura5.png" alt="A interface gráfica do programa heimdall aberto no computador. Fonte: O autor (2019)" caption="Figura 5 - Interface gráfica do Heimdall">}}

Sim, o Heimdall precisa de root para funcionar, igual o Odin precisa de
administrador no Windows, a não ser que você mude as permissões da porta USB no
seu Linux, o que dará muito trabalho.

É isso, espero ter sido útil ao mostrar que existe uma alternativa viável e que
dá para modificar o seu celular Galaxy sem recorrer mais ao Odin.

# Bibliografia

Site oficial do Heimdall - Glass Echidna (perguntas frequentes):

[Heimdall Glass Echidna](https://www.glassechidna.com.au/heimdall/)

Documentação do Heimdall para Linux - Apêndice B:

[BenjaminDobell/Heimdall/README](https://gitlab.com/BenjaminDobell/Heimdall/raw/master/Linux/README)
