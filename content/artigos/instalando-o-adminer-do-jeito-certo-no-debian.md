+++
title = "Instalando o adminer do jeito certo no Debian"
date = "2020-09-21"
author = "Cézar Campos"
description = "Qual é o melhor Linux para computadores antigos? Basicamente qualquer um, desde que você use um gerenciador de janelas como esse."
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado no
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/Instalando-o-Adminer-do-jeito-certo-no-Debian/)

O Adminer, formalmente conhecido como PhpMinAdmin, é uma ferramenta para
administração de banco de dados que trabalha no contexto WEB. Muitos tutoriais
por aí ensinam a instalar o Adminer direto do código-fonte, em um único arquivo,
sem mencionar que existe o pacote do mesmo programa para o Debian com todas as
configurações prontas.

Neste tutorial, eu vou mostrar como instalar e configurar o Adminer no estilo
Debian.

# Instalação do pacote

Vamos assumir que você já tenha o Apache2 e algum banco de dados instalados,
caso não tenha, eu sugiro que a instalação seja feita por esse comando:

```console
# apt-get install default-mysql-server apache2
```

Agora, instale o Adminer com esse comando:

```console
# apt-get install adminer --yes-install-recommends
```

Durante a instalação, o Adminer será compilado em um único arquivo, arquivo esse
que você encontra no local _/usr/share/adminer/adminer.php_. Não mexa neste
arquivo.

# Configuração do apache

O pacote Adminer traz, por padrão, um arquivo de configuração pronto para ser
usado com o servidor Apache, ele vai estar nesse local:
_/etc/apache2/conf-available/adminer.conf_.

Para fazer com que o Apache utilize esse arquivo, dê os seguintes comandos:

```console
# a2enconf adminer
# systemctl reload apache2
```

A partir de agora, você pode acessar a interface do Adminer no seu navegador a
partir do endereço: **http://localhost/adminer**

Se você quiser, você pode mudar a URL padrão, por questões de segurança, a
partir do arquivo de configuração que acabamos de habilitar. Abra-o e altere o
alias para a URL desejada.

```console
# vim /etc/apache2/conf-enabled/adminer.conf
```

```
Alias /URL_IMPOSSIVEL_DE_ACHAR /etc/adminer
<Directory /etc/adminer>
	Require all granted
	DirectoryIndex conf.php
</Directory>
```

Após a alteração desse arquivo, recarregue o Apache.

```console
# systemctl reload apache2
```

{{< figure src="/artigos/instalando-o-adminer-do-jeito-certo-no-debian/figura1.png" alt="Captura da tela do adminer. Fonte: O autor (2020)" caption="Figura 1 - Captura de tela do adminer" >}}

# Perfumaria

Se você é um camarada que gosta de embelezar as coisas, você pode baixar temas
customizados para a interface do seu Adminer. Para isso, vá até o site:

[Adminer - Database management in a single PHP file](https://www.adminer.org/)

...e baixe o CSS do tema que mais lhe agrada. No meu caso, eu baixei o tema
Pepa-linha-dark pois é o tema que possui o visual mais consistente.

```console
# wget https://raw.githubusercontent.com/pepa-linha/Adminer-Design-Dark/master/adminer.css
```

Na documentação, diz que você deve colocar o "adminer.css" do lado do
"adminer.php", porém isso não funciona no Debian. O local correto é o
/etc/adminer, onde fica o arquivo "conf.php":

```console
# mv adminer.css /etc/adminer/
```

Se você atualizar a página, vai ver que o Adminer agora está customizado.

{{< figure src="/artigos/instalando-o-adminer-do-jeito-certo-no-debian/figura2.png" alt="Imagem do adminer customizado. Fonte: O autor (2020)" caption="Figura 2 - Adminer customizado" >}}

É isso, para fazer login no Adminer, crie um usuário no banco de dados e dê a
ele as permissões necessárias para fazer o login, lembrando que não é
recomendado entrar como root, pois pode expor muita coisa sobre o teu banco de
dados na WEB.

# Fontes

Arquivos de documentação no pacote do Adminer:

- /usr/share/doc/adminer/readme-designs.txt
- /usr/share/doc/adminer/readme.txt
- /usr/share/doc/adminer/README.Debian

Testado no Debian Bullseye.
