+++
title = "Tema escuro no Elementary sem programas"
date = "2021-09-27"
author = "Cézar Campos"
description = "Nessa dica eu ensinarei como habilitar o tema escuro no Elementary OS sem usar programas de terceiros."
tags = ["Dica vivaolinux"]
+++

Antigamente as pessoas instalavam programas como o
[Elementary Tweaks](https://github.com/elementary-tweaks/elementary-tweaks) ou o
[Pantheon Tweaks](https://github.com/pantheon-tweaks/pantheon-tweaks/) para
customizar a área de trabalho do Elementary OS.

Se o seu objetivo é só usar o tema escuro nativo do sistema então você não
precisa instalar nenhum desses programas, de fato, tudo o que você precisa já
está instalado!

Existem duas formas de você habilitar o modo escuro e isso funciona em qualquer
interface do estilo **GTK**.

## 1. Arquivo de configuração

Abra a pasta home do seu usuário com o gerenciador de arquivos, pressione
`Ctrl + H` para exibir os arquivos ocultos e entre na pasta
**~/.config/gtk-3.0**

{{< figure src="/artigos/tema-escuro-no-elementary-sem-programas/figura1.png" alt="Imagem do gestor de arquivos aberto na pasta GTK 3.0. Fonte: O autor (2021)" caption="Figura 1" captionPosition="center" position="center" >}}

Clique com o botão direito do mouse e crie um novo arquivo chamado
_settings.ini_.

Abra esse arquivo com algum editor de texto e cole o seguinte conteúdo dentro
dele:

```ini
[Settings]
aplication-prefer-dark-theme=true
```

{{< figure src="/artigos/tema-escuro-no-elementary-sem-programas/figura2.png" alt="Imagem do editor de texto aberto. Fonte: O autor (2021)" caption="Figura 2" captionPosition="center" position="center" >}}

Salve, feche o arquivo e pronto! Os seus aplicativos agora vão usar o tema
escuro nativo do Elementary OS por causa dessa configuração, para que as
mudanças tenham efeito feche e abra os aplicativos.

{{< figure src="/artigos/tema-escuro-no-elementary-sem-programas/figura3.png" alt="Um navegador de internet mostrando que está pedindo um tema escuro para todos os sites. Fonte: O autor (2021)" caption="Figura 3 - Navegador do Elementary com o tema escuro nativo" captionPosition="center" position="center" >}}

Olhe como o navegador Epiphany ficou! Como efeito colateral o navegador também
diz para os websites que você quer usar um tema escuro e isso é bom porque os
sites ficam com o visual mais consistente igual o sistema.

{{< figure src="/artigos/tema-escuro-no-elementary-sem-programas/figura4.png" alt="Imagem do gestor de arquivos com a estilização escura. Fonte: O autor (2021)" caption="Figura 4 - Gestor de arquivos com o tema escuro" captionPosition="center" position="center" >}}

O gerenciador de arquivos também ficou escuro após fechar e abrir ele.

Agora com a advento do GTK4.0 se você quiser que os novos aplicativos fiquem com
o tema escuro você deverá fazer o mesmo passo a passo acima porém na pasta
`~/.config/gtk-4.0`.

## 2. Através de uma variável de ambiente

Esse truque é antigo, mas muita gente esquece que dá para usar ele para
customizar o desktop de uma maneira mais universal, se você seguiu o método
acima deve ter percebido que alguns aplicativos ainda não ficaram com o tema
escuro, como por exemplo as configurações do sistema.

{{< figure src="/artigos/tema-escuro-no-elementary-sem-programas/figura5.png" alt="Imagem das configurações do sistema ainda utilizando o tema padrão claro. Fonte: O autor (2021)" caption="Figura 5 - Configurações do sistema com o tema claro" captionPosition="center" position="center" >}}

Para forçar todos os aplicativos a usarem o tema escuro você deve definir uma
variável de ambiente dizendo qual tema o GTK deve usar, lembrando que esse
método é o menos recomendado pois é uma gambiarra.

Abra o terminal e logue-se como usuário root.

```console
$ sudo su -
```

Agora abra o arquivo **/etc/environment** com o seu editor de texto favorito, no
meu caso é o vi:

```console
# vi /etc/environment
```

Se o arquivo não existir crie-o. Dentro do arquivo coloque a seguinte variável
de ambiente:

```bash
GTK_THEME="elementary:dark"
```

Salve e reinicie o computador. Agora todos os aplicativos que se recusam a ficar
escuros ficarão com o tema escuro, até mesmo as configurações do sistema:

{{< figure src="/artigos/tema-escuro-no-elementary-sem-programas/figura6.png" alt="Imagem das configurações do sistema com o tema escuro. Fonte: O autor (2021)" caption="Figura 6 - Configurações do sistema com o tema escuro" captionPosition="center" position="center" >}}

É isso, espero que essa dica ajude o pessoal a ter uma experiência melhor usando
o Elementary OS.
