+++
title = "Você deixa a porta aberta?"
date = "2019-04-10"
author = "Cézar Campos"
description = "Aqui eu relato sobre a falha de segurança que descobri na empresa que eu trabalho, essa falha foi corrigida e serve de inspiração para outras pessoas."
tags = ["Dica vivaolinux"]
+++

Estava eu em um belo dia monitorando os logs do _PfSense_ da organização onde eu
trabalho, até que eu encontro um safadinho xing-ling tentando logar no Firewall
via SSH.

```
Data					protocol	PID			Descrição
Mar 31 08:46	sshd			87004		Invalid user ethos from 111.202.xxx.13
Mar 31 08:46	sshd			87004		input_userauth_request: invalid user ethos [preauth]
Mar 31 08:46	sshd			87004		Failed password for invalid user ethos from 111.202.xxx.13 port 62274 ssh2
Mar 31 08:46	sshd			87004		Connection closed by 111.202.xxx.13 port 62274 [preauth]
Mar 31 08:49	sshd			2536		Failed password for root from 111.202.xxx.13 port 19498 ssh2
Mar 31 08:49	sshd			2536		Connection closed by 111.202.xxx.13 port 19498 [preauth]
Mar 31 08:53	sshd			9984		Invalid user oracle from 111.202.xxx.13
```

Claro que ele ficou o dia inteiro tentando diversos nomes de usuários até achar
um nome de usuário que fosse válido, para daí tentar diversas senhas diferentes.
Mas como o brasileiro é um ser bastante troll, o nome de usuário e senha possuem
caracteres especiais. rsrs

Como eu não costumo acessar o Firewall via SSH nem dentro e nem fora da
organização, percebi que outras pessoas poderiam tentar invadir dessa forma
através do serviço que eu nem uso. Então para resolver este buraco na segurança,
o acesso via SSH foi desabilitado e as portas 22, 62274 e etc foram fechadas.

Além disso, usar caracteres especiais do nosso idioma como o cedilha por exemplo
ajuda a impedir que gringos consigam acertar seu nome de usuário e muito menos a
senha.

A lição que fica é, desabilite os serviços que você não usa, feche as portas da
sua rede onde potenciais invasores poderiam usar para acessar sua máquina e o
mais importante, se você tiver um servidor de ssh instalado na sua distro Linux
pessoal, saiba que com o seu o IP é possível que alguém tente invadir por essa
mesma porta.

Até a próxima =)
