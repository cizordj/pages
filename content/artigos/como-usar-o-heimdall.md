+++
title = "Como usar o Heimdall"
date = "2019-03-24"
author = "Cézar Campos"
description = "Como instalar ROMs celulares da Samsung usando o Linux usando uma ferramenta de código aberto chamada Heimdall."
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado em
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/como-usar-o-heimdall).

# Preparação

Se você não sabe o que é, ou não tem o _Heimdall_ instalado, leia o artigo:
[Introdução ao Heimdall](/artigos/introducao-ao-heimdall)

# Introdução

No mundo do Android, as pessoas que possuem um celular da _Samsung_ se acostumam
a utilizar o Odin para fazer a instalação da _Stock ROM_ e custom recoveries.
Isso é até normal, pois toda a comunidade faz isso e os tutoriais, em sua
maioria, se voltam ao uso do Odin por ser a ferramenta oficial da Samsung.

Porém, quando se trata de distribuições Linux, o Odin não funciona (pois não foi
feito para Linux) e os usuários não têm o que fazer, senão a recorrer a algum
computador com o Windows para fazer isso.

Agora que temos o Heimdall como alternativa, vamos aprender agora como se faz
para instalar uma Stock ROM no seu Galaxy.

# Download da stock ROM

Este é o passo que, em minha opinião, é o mais chato do artigo, visto que o
download pode demorar algumas horas, pois todas as distribuidoras de firmware
adoram limitar a banda de rede para os usuários gratuitos A não ser que você
vire usuário premium, a taxa de download pode beirar alguns Kilobytes por
segundo.

Vá para o site [PT | SamDb.org](https://samdb.org/pt/) e escreva na caixa de
pesquisa o modelo exato do seu aparelho. Para saber o modelo do seu celular
Android, vá em: Configurações → Sobre o dispositvo → Modelo do dispositivo.

Neste artigo usaremos como exemplo um Galaxy Grand Prime, cujo modelo é
SM-G531H.

{{< figure src="/artigos/como-usar-o-heimdall/figura1.png" alt="Captura de tela do site samdb.org. Fonte: O autor (2019)" caption="Figura 1 - Captura de tela do site" >}}

Nesse site, digite e clique no resultado que aparecer do seu aparelho e não dê
Enter. Agora, feita a pesquisa, vamos escolher um firmware para o nosso
aparelho.

Obs.: Existe a questão de nem todas as bandas de base serem compatíveis, mas
para não complicar o artigo, escolha sempre a ROM do seu país, Ok?

{{< figure src="/artigos/como-usar-o-heimdall/figura2.png" alt="Imagem do site listando todos os tipos de ROM para o celular especificado. Fonte: O autor (2019)" caption="Figura 2 - Lista de ROMs para o Galaxy Gran Prime" >}}

Neste caso, escolhi a variante ZTO mais recente do Brasil, pois ela é uma ROM
neutra. As variantes ZTA e ZTR acompanham bloatwares das operadoras.

Na página de download, escolha a opção "Velocidade Lenta" e aguarde 60 segundos,
se não aparecer um contador de 60 segundos, o jeito é se registrar mesmo. No meu
caso não precisou, mas se precisar é só fazer uma conta e logar para efetuar o
download.

Vai aparecer uma tela escrito "Velocidade lenta" de novo, você pode clicar nela
para começar o download.

Opcionalmente, você pode querer usar o wget, visto que erros podem acontecer
durante o download e o wget é mais firme para baixar.

{{< figure src="/artigos/como-usar-o-heimdall/figura3.png" alt="Imagem mostrando o diálogo de transfêrencia da ROM no site. Fonte: O autor (2019)" caption="Figura 3 - Diálogo de transferência da ROM" >}}

Para isso, clique com o botão direito do mouse sobre o "Velocidade lenta" e
copie o link, tal como na imagem acima e cole no terminal:

```console
$ wget <link que você copiou> -O SM-G531H-Stock.zip
```

Agora, vá tomar um café, ou tirar um cochilo, pois o download demorará bastante.

# Preparação do arquivo

Uma vez que o arquivo tenha sido baixado, é hora de mexer com ele para funcionar
no Heimdall. No Windows com o Odin, basta você extrair o ".zip" e usar o arquivo
tal como está, porém no Linux um esforcinho a mais é necessário.

Pegue o seu arquivo e extraia.

```console
$ unzip SM-G531H-Stock.zip
```

```
Archive: SM-G531H-Stock.zip inflating:
G531HVJS0ARI1_G531HZTO0ARH2_G531HUBU0AQI1_HOME.tar.md5
```

Note que o nome do arquivo termina com ".tar.md5". Pois bem, renomeie o arquivo
tirando o ".md5" no final.

```console
$ mv G531HVJS0ARI1_G531HZTO0ARH2_G531HUBU0AQI1_HOME.tar.md5 G531HVJS0ARI1_G531HZTO0ARH2_G531HUBU0AQI1_HOME.tar --verbose
```

```
'G531HVJS0ARI1_G531HZTO0ARH2_G531HUBU0AQI1_HOME.tar.md5' -> 'G531HVJS0ARI1_G531HZTO0ARH2_G531HUBU0AQI1_HOME.tar'
```

Agora que ele é um arquivo ".tar", podemos extraí-lo de novo.

```console
$ tar -xvf G531HVJS0ARI1_G531HZTO0ARH2_G531HUBU0AQI1_HOME.tar
```

```
boot.img
recovery.img
system.img
cache.img
hidden.img
SPRDCP.img
SPRDDSP.img
```

Dava para fazer isso via interface gráfica também, sem problemas.

Note que saiu um monte de arquivos com a extensão ".img", é com eles que o nosso
programa Heimdall vai trabalhar. É bom você salvar esses arquivos em uma pasta
separada, para fins de organização juntamente com o arquivo ".pit" que veremos
adiante.

# Extração do Arquivo PIT

Obs.: Antes de começar, se o seu celular tiver FAP LOCK, vá nas opções do
desenvolvedor e ative a opção "Desbloquear por OEM" antes de prosseguir.

{{< figure src="/artigos/como-usar-o-heimdall/figura4.png" alt="Imagem de um celular desligado em modo de download. Fonte: O autor (2019)" caption="Figura 4 - Celular em modo download">}}

É agora que colocamos o celular em modo download, desligue o aparelho e
pressione os botões POWER + VOLUME BAIXO + HOME por alguns segundos até vibrar.
Ignore o aviso pressionando VOLUME CIMA para continuar. Conecte o seu celular ao
computador com um cabo USB (de qualidade) e então partiremos ao Heimdall.

# Via Interface Gráfica

Comecemos pela interface gráfica. Digite em um terminal qualquer para lançar o
Heimdall.

```console
$ sudo heimdall-frontend
```

Vá na aba: "Utilities"

{{< figure src="/artigos/como-usar-o-heimdall/figura5.png" alt="Programa de computador aberto na aba “Utilities”. Fonte: O autor (2019)" caption="Figura 5 - Interface gráfica do Heimdall">}}

Para testar a conexão, clique em Detect Device, se aparecer "Device detected",
então está tudo certo. Na área escrita "Download PIT" tem um botão escrito "Save
As", clique lá para escolher aonde salvar o seu arquivo PIT. E por último clique
em "Download".

{{< figure src="/artigos/como-usar-o-heimdall/figura6.png" alt="Programa de computador executando o comando de extração do arquivo PIT. Fonte: O autor (2019)" caption="Figura 6 - Interface gráfica do Heimdall">}}

"Pit file download successful" é o que deve aparecer. Confira aonde você salvou
o arquivo PIT para ter certeza de que ele não tem 0 kbytes, se tiver apague-o e
repita o procedimento.

# Via linha de comando

Se você é uma daquelas pessoas que preferem o terminal, bastava apenas digitar:

```console
$ sudo heimdall download-pit --output arquivo.pit
```

{{< figure src="/artigos/como-usar-o-heimdall/figura7.png" alt="Programa de computador executando o comando de extração do arquivo PIT. Fonte: O autor (2019)" caption="Figura 7 - Interface de linha de comando do Heimdall">}}

Após isso, o arquivo será salvo no diretório atual.

É bom você fazer um becape deste arquivo para usos futuros, quem sabe... pois
vamos precisar dele adiante.

# Instalação da stock ROM

É agora que vem o momento mais importante, é quando vamos instalar a stock ROM
do seu aparelho. Você já tem o arquivo ".pit" e os outros arquivos ".img"
naquela pasta. Primeiro, vou mostrar em interface gráfica e depois por linha de
comando.

Coloque o seu aparelho em modo download, conecte ao computador e abra o Heimdall
com esse comando:

```console
$ sudo heimdall-frontend
```

{{< figure src="/artigos/como-usar-o-heimdall/figura8.png" alt="Programa de computador aberto na aba de “flash”. Fonte: O autor (2019)" caption="Figura 8 - Interface gráfica do Heimdall na aba “flash”">}}

Na seção "PIT" existe o botão "Browse", então clique ali para selecionarmos o
arquivo ".pit" gerado anteriormente. Com o arquivo ".pit" selecionado, comecemos
a selecionar os arquivos ".img" para cada partitição.

Agora vem a parte boa, na seção "Partition Files" clique em "Add".

{{< figure src="/artigos/como-usar-o-heimdall/figura9.png" alt="Trecho da interface gráfica mostrando os detalhes da particação do celular como o nome, id e o arquivo de imagem que vai nela. Fonte: O autor (2019)" caption="Figura 9 - Detalhes da partição">}}

Agora a seção "Partition Details" vai te dar a opção de escolher uma "Partition
Name", ou nome da partição e note que logo abaixo está escrito: "File"
(boot.img) e depois "Browse". O que quer dizer, é que você deve selecionar o
arquivo "boot.img" de um daqueles arquivos ".img" que você descompactou
anteriormente. Este arquivo será queimado na partição KERNEL.

Agora, clique em "Add" de novo e selecione outro nome de partição, é claro que
eles não vão estar em ordem igual aqui, então você vai ter que selecionar uma
partição por vez até o parâmetro "File" mostrar um nome de arquivo que você
possua. Como no meu caso, foram só sete arquivos, você não vai preencher todas
as trinta partições.

{{< figure src="/artigos/como-usar-o-heimdall/figura10.png" alt="Trecho da interface gráfica mostrando os detalhes da CACHE do celular. Fonte: O autor (2019)" caption="Figura 10 - Detalhes da partição CACHE">}}

Preste bem atenção nos nomes dos arquivos que você está flasheando nas
partições, não vá colocar o "boot.img" na partição RECOVERY por exemplo. Vá
adicionando partições até que todos os arquivos ".img" que você baixou estejam
selecionados.

A sua tela "Partition Files" deve estar mais ou menos assim:

{{< figure src="/artigos/como-usar-o-heimdall/figura11.png" alt="Trecho da interface gráfica mostrando uma lista de arquivos a serem instalados no celular. Fonte: O autor (2019)" caption="Figura 11 - Arquivos de partição selecionados">}}

Se você adicionou alguma partição por engano, selecione-a e depois clique em
REMOVE.

Tudo preenchido, nenhum arquivo faltando, basta dar um START.

{{< figure src="/artigos/como-usar-o-heimdall/figura12.png" alt="Trecho da interface gráfica mostrando o botão de iniciar do Heimdall. Fonte: O autor (2019)" caption="Figura 12 - Botão de “start” da interface do Heimdall">}}

Se tudo der certo, o seu telefone deve reiniciar com a nova stock rom instalada.
=)

# Via terminal

A instalação pelo terminal é mais direto ao ponto. A principal diferença, é que
ao invés de selecionarmos os arquivos pelo mouse um por um, nós vamos passar
todos eles através de ARGUMENTOS pela linha de comando.

Com o celular em modo download conectado ao PC, faça um teste de conexão:

```console
# heimdall detect
```

```
Device detected
```

Tudo certo, então vamos instalar a stock rom:

```console
# heimdall flash --pit arquivo.pit --KERNEL boot.img --CACHE cache.img --HIDDEN hidden.img --RECOVERY recovery.img --WDSP SPRDDSP.img --MODEM SPRDCP.img --SYSTEM system.img
```

```
. . .
Uploading KERNEL
100%
KERNEL upload successful

Uploading CACHE
100%
CACHE upload successful
. . .
```

Se a sua saída de texto for mais ou menos assim e der sucesso em todas as
partições, o seu telefone vai reiniciar já na stock ROM. =)

# Explicação

O parâmetro "--pit" deve ser procedido do local onde está o arquivo ".pit" e os
demais são os nomes das partições e seus arquivos: "--RECOVERY recovery.img
--KERNEL boot.img --WDSP SPRDDSP.img..."

Onde:

- "--RECOVERY" é a partição recovery do seu celular;
- "recovery.img" é o arquivo de imagem que será gravado nela.

Você pode um dia se perguntar, mas e esses nomes complicados? "--WDSP
SPRDDSP.img"

Como vou saber o nome de cada partição onde cada arquivo vai? Bem, pequeno
gafanhoto, aí você perguntou demais, mas vamos ao trabalho.

O arquivo ".pit" que você extraiu vai ser útil, digite no terminal:

```console
$ heimdall print-pit --file arquivo.pit
```

Ele vai imprimir um texto enorme detalhando todas as trinta partições do
celular, é em cada bloco de texto que eu vejo o nome da partição e o arquivo que
vai nela. Isso tem que ser anotado para depois ser passado como argumento ao
Heimdall. Por exemplo:

```
--- Entry #29 ---
Binary Type: 0 (AP)
Device Type: 2 (MMC)
Identifier: 26
Attributes: 5 (Read/Write)
Update Attributes: 5 (FOTA)
Partition Block Size/Offset: 3710976
Partition Block Count: 409600
File Offset (Obsolete): 0
File Size (Obsolete): 0
Partition Name: HIDDEN Flash
Filename: hidden.img
FOTA Filename:
```

Veja que na linha "Partition Name" está "HIDDEN" toda em maiúscula e embaixo
"hidden.img" indicando o arquivo a ser instalado. O argumento fica assim:
"--HIDDEN hidden.img"

Ou, pode ser o número da partição. O Heimdall também aceita: "--26 hidden.img"

E assim você vai juntando argumentos até o comando ficar completo:

```console
# heimdall flash --pit arquivo.pit --KERNEL boot.img --CACHE cache.img --HIDDEN hidden.img --RECOVERY recovery.img --WDSP SPRDDSP.img --MODEM SPRDCP.img --SYSTEM system.img
```

É assim que se faz pelo terminal, pequenos gafanhotos. :wink:

Bom, se o seu objetivo era só instalar a Stock ROM, o artigo acaba por aqui. Mas
se você quiser explorar mais uma funcionalidade do Heimdall, passe para a
próxima página.

# Como criar um Heimdall package

Isso tudo seria mais fácil se fosse que nem no Odin, onde você só escolhe o
arquivo e instala, nada de comandos, arquivo ".pit" e partições do celular. Pois
bem, o Heimdall também é capaz de fazer isso se todas as distribuidoras de
firmware distribuíssem Heimdall packages, ou pacotes do Heimdall. Com eles, você
só carrega o pacote na aba Load Package do Heimdall e estaria tudo certo. Porém,
como ninguém distribui esses pacotes, você pode ser um dos primeiros a fazer
isso e facilitar a vida de outros usuários do Heimdall que possuem o mesmo
aparelho que você.

Aproveitando o embalo que você instalou a stock ROM pela interface gráfica do
Heimdall e tem todos os arquivos e partições selecionados, você pode querer
criar um pacote para si mesmo no futuro, ou para outros usuários.

É muito simples, do jeito que o programa estava na hora que você instalou a
stock ROM pela interface gráfica, todos os arquivos ".img" para suas respectivas
partições, a aba "Create Package" vai se tornar clicável.

É obrigatório ter o nome de um desenvolvedor e o nome de um aparelho suportado.
Se for apenas para uso pessoal, você pode preencher qualquer coisa, mas se você
pretende distribuir internet à fora, é no mínimo ÉTICO que você preencha todas
as informações corretamente para evitar bricks desnecessários de outras pessoas.

{{< figure src="/artigos/como-usar-o-heimdall/figura13.png" alt="Interface gráfica do Heimdall com opções para se criar um pacote. Fonte: O autor (2019)" caption="Figura 13 - Interface de criação de pacotes do Heimdall">}}

No campo "Firmware Name" é ideal que você coloque o mesmo nome que veio quando
você baixou a ROM pela primeira vez. Este nome usualmente é a versão da banda de
base usado para identificar ROMs únicas de cada país e operadoras.

{{< figure src="/artigos/como-usar-o-heimdall/figura14.png" alt="Trecho da interface gráfica do Heimdall onde você pode especificar o modelo do celular. Fonte: O autor (2019)" caption="Figura 14 - Especificação do modelo do celular">}}

Na aba "Device Info" você vai preencher os dados do aparelho à qual esta ROM
pode ser instalada. No "Product Code" se coloca o modelo do aparelho (conforme a
documentação pede).

Após todas as informações serem preenchidas, o botão "Build" vai se tornar
clicável e é aí que o pacote é criado.

Você então escolhe um nome para o arquivo e a pasta onde ele será salvo.

{{< figure src="/artigos/como-usar-o-heimdall/figura15.png" alt="Barra de progresso do Heimdall onde o pacote está sendo criado. Fonte: O autor (2019)" caption="Figura 15 - Pacote sendo criado">}}

Mais tarde, se você quiser usar este pacote, você pode descarrega-lo pela aba
<i>Load Package</i> do Heimdall.

É isso.

# Conclusão

Pronto! Agora você sabe como fazer um monte de coisas no seu Samsung Galaxy sem
depender mais do Odin para isso! É recomendável que você não instale binários de
terceiros, caso seu aparelho ainda esteja na garantia.

O autor se exime da responsabilidade por qualquer soft-brick, guerra nuclear e
fim do mundo e como sabemos, você foi avisado.

Espero ter ajudado aqueles que perguntaram no VOL antigamente e não havia uma
resposta na época e também espero ajudar os novos usuários que migram para o
Linux e são entusiastas do Android (assim como eu) a se virar com as ferramentas
que tem. Qualquer dúvida é só perguntar =-).

Para todos um forte abraço.

# Bibliografia

Muita tentativa e erro, muito soft-brick, muita leitura e participação em fóruns
estrangeiros. Além disso, a leitura da documentação do Heimdall é SUPER
recomendada, mexo com o meu celular Android desde 2014 e até hoje continuo
fuçando nas configurações dele.

Documentação do Heimdall para Linux → Apêndice A:

[Linux - master - Benjamin Dobell / Heimdall - GitLab](https://gitlab.com/BenjaminDobell/Heimdall/tree/master/Linux)
