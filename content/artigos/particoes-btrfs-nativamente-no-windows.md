+++
title = "Partições BTRFS nativamente no Windows"
date = "2019-11-25"
lastmod = "2021-02-02"
author = "Cézar Campos"
description = "Aqui eu mostro como se faz para acessar as partições Linux com o sistema de arquivos BTRFS através do Windows"
tags = ["Dica vivaolinux"]
language = "pt-br"
+++

Com a crescente utilização do _BTRFS_ como sistema de arquivos padrão nas
distribuições Linux, houveram algumas iniciativas em trazer o mesmo suporte ao
sistema operacional de código fechado Microsoft Windows.

Para algumas pessoas que usam dual boot, pode ser bom ter acesso às partições
Linux de forma mais simples sem usar programas de terceiros, como por exemplo o
[Linux Reader](https://www.diskinternals.com/linux-reader/features/).

Estava há algum tempo perambulando no GitHub, até que achei o seguinte driver
feito pelo usuário @maharmstone, o
[WinBTRFS](https://github.com/maharmstone/btrfs#winbtrfs-v14). Este driver
"ensina" o sistema operacional ler as partições Linux a nível de Kernel, ou
seja, você pode acessar usando o Explorer normalmente, como se fosse uma pasta
qualquer.

Para instalá-lo é bem simples, você vai baixar a última liberação por este link:

[https://github.com/maharmstone/btrfs/releases](https://github.com/maharmstone/btrfs/releases)

Como eu baixei a versão 1.4, então veio a seguinte pasta:
{{< figure src="/artigos/particoes-btrfs-nativamente-no-windows/figura1.png" alt="Ilustração de uma pasta compactada no Windows com o nome btrfs 1.4. Fonte: O autor (2019)" caption="Figura 1" position="center" captionPosition="center" >}}

Você vai descompactá-la e dar dois cliques no arquivo "btrfs.ini", com o botão
direito do mouse:

{{< figure src="/artigos/particoes-btrfs-nativamente-no-windows/figura2.png" alt="Captura de tela mostrando o menu de contexto ao clicar sobre o arquivo btrfs.ini dando destaque a opção de instalar. Fonte: O autor (2019)" caption="Figura 2" position="center" captionPosition="center" >}}

Quando acabar a instalação, você terá que reiniciar o Windows para que o driver
comece a funcionar.

{{< figure src="/artigos/particoes-btrfs-nativamente-no-windows/figura3.png" alt="Caixa de diálogo do Windows pedindo para reiniciar o computador afim de aplicar as alterações. Fonte: O autor (2019)" caption="Figura 3" position="center" captionPosition="center" >}}

Depois de reiniciar, você verá as partições Linux em "Meu Computador", como se
fossem partições normais do Windows.

{{< figure src="/artigos/particoes-btrfs-nativamente-no-windows/figura4.png" alt="Captura de tela do Windows sublinhando as novas partições de disco que aparecem em “Meu Computador”. Fonte: O autor (2019)" caption="Figura 4" position="center" captionPosition="center" >}}

Agora com acesso facilitado às partições Linux, é recomendável abrir o olho
quanto aos possíveis ataques de ransomware.

É isso, espero que isso te ajude a ter mais produtividade no dia a dia,
especialmente àqueles que possuem dual boot entre os dois sistemas operacionais.

## Bibliografia

Documentação do WinBTRFS:

[btrfs/README.md at master · maharmstone/btrfs · GitHub](https://github.com/maharmstone/btrfs/blob/master/README.md)

ATENÇÃO: o autor desta dica e o autor do driver, se eximem da responsabilidade
por quaisquer danos ao sistema de arquivos. Use por sua conta e risco.
