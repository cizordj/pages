+++
title = "Um manual mais da hora"
date = "2021-04-13"
lastmod = "2021-12-03"
author = "Cézar Campos"
description = "Como ler manuais no Linux de forma mais interesante! Aqui eu apresento métodos simples para deixar a sua leitura mais agradável e excitante."
tags = ["Dica vivaolinux"]
+++

Você está cansado de ler manuais no Linux? Você acha maçante ler manuais
preto e branco? Então essa dica é para você, meu camarada nutella.

{{< figure src="/artigos/um-manual-mais-da-hora/figura1.png" alt="Uma captura de tela mostrando o manual do groff em preto e branco no terminal do Linux. Fonte: O autor (2021)" caption="Figura 1 - Programa less sendo usado como leitor de manuais padrão" position="center" captionPosition="center">}}

Para quem gosta de perfumaria (como eu), existem duas alternativas para
deixar a leitura de manuais no Linux mais agradável e elas vão
impressionar você devido a tamanha facilidade que é configurar.

## Passos

### 1. Most
{{< figure src="/artigos/um-manual-mais-da-hora/figura2.png" alt="Uma captura de tela mostrando o manual do groff colorido no terminal do Linux. Fonte: O autor (2021)" caption="Figura 2 - Programa most sendo usado como leitor de manuais padrão" position="center" captionPosition="center">}}

Para usar o most como o programa padrão para ler manuais, primeiro é
preciso instalá-lo:

```console
# apt-get install most
```

E depois defini-lo como leitor padrão através de uma variável de
ambiente. Coloque o seguinte texto dentro de
_/etc/environment:_

```console
MANPAGER="/usr/bin/most"
```
### 2. NeoVim
{{< figure src="/artigos/um-manual-mais-da-hora/figura3.png" alt="Uma captura de tela mostrando o manual do groff colorido no terminal do Linux. Fonte: O autor (2021)" caption="Figura 3 - Programa nvim sendo usado como leitor de manuais padrão" position="center" captionPosition="center">}}

Se você é um camarada mais acostumado com o NeoVIM, então vai ser fácil
defini-lo como leitor padrão de manuais. Basta você adicionar o seguinte
dentro do arquivo _/etc/environment:_

```console
MANPAGER='nvim +Man!'
```

- Testado no Debian
- A configuração de variáveis de ambiente pode variar em outras distribuições

É isso pessoal, até a próxima dica.
