+++
title = "i3 para computadores antigos"
date = "2020-08-19"
author = "Cézar Campos"
description = "Bom dia a todos do VOL, hoje vou mostrar como vocês podem usar uma interface minimalista e bela para dar vida a um computador antigo de um jeito bem simples. Aqui nessa dica eu mostro como fiz para ressuscitar um notebook Acer Aspire 5570Z de 32 bits e usá-lo no meu dia a dia como programador de sites."
tags = ["Artigo vivaolinux"]
language = "pt-br"
+++

Originalmente publicado no
[vivaolinux.com.br](https://www.vivaolinux.com.br/artigo/i3-para-computadores-antigos/)

# Instalação do ambiente

Bom dia a todos do VOL, hoje vou mostrar como vocês podem usar uma interface
minimalista e bela para dar vida a um computador antigo de um jeito bem simples.

Aqui nessa dica eu mostro como fiz para ressuscitar um _notebook Acer Aspire
5570Z_ de 32 bits e usá-lo no meu dia a dia como programador de sites.

O _i3_ é um gerenciador de janelas conhecido pelo seu fluxo de trabalho
orientado a teclado, isto é, nenhum mouse é necessário para a maioria das
aplicações, além disso o i3wm consome poucos recursos de hardware e é muito
customizável.

Tendo isso em mente vamos instalar o
[Debian testing non-free](https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/weekly-builds/)
sem interface gráfica no computador. Após o primeiro boot entre como usuário
root e faça aquela rotina de atualizações básicas antes de começar:

```console
# apt-get update
# apt-get upgrade
```

Agora precisamos instalar o i3 e um gerenciador de login. De todos os DM's, os
mais simples que conheço são o Xdm e o Lightdm. Recomendo mais o Lightdm para
não dar dor de cabeça, porém eu instalei o XDM no meu.

```console
# apt-get install i3 xdm
```

O xdm precisa ser precisa ser configurado pra iniciar o i3, então edite o
seguinte arquivo:

```console
# editor /etc/X11/xdm/Xsession
```

Adicione 'i3' no final do arquivo e agora reinicie o computador:

```console
# systemctl reboot
```

Após a reinicialização a tela de login será apresentada a você, entre com o seu
usuário e senha.

# Customização

Na primeira vez que você iniciar a sessão no i3 ele vai estar mais ou menos
desse jeito:

{{< figure src="/artigos/i3-para-computadores-antigos/figura1.png" alt="Tela branca. Fonte: O autor (2020)" caption="Figura 1 - Captura de tela do i3 sem modificações">}}

O programa que vamos usar para customizar o i3 chama-se _i3-themer_, esse script
contém uma coleção de temas que você pode usar no seu ambiente.

{{< figure src="/artigos/i3-para-computadores-antigos/figura2.gif" alt="Um emanharado de capturas de telas mostrando temas com diferentes cores. Fonte: O autor (2020)" caption="Figura 2 - Temas disponíveis">}}

Para instalar o programa, instale o git e baixe o repositório:

```console
$ sudo apt-get install git
$ git clone https://github.com/cizordj/i3-themer.git --depth=1
```

Instale as dependências recomendadas pelo i3-themer:

```console
$ sudo apt-get install dunst polybar nitrogen rofi bash fonts-font-awesome xdg-utils sensible-utils alsa-utils rxvt-unicode
```

Além disso, para uma experiência completa instale os pacotes que vêm no próprio
repositório do i3-themer:

```console
$ cd i3-themer/assets/debian_dependencies/
$ sudo apt-get install ./*.deb
```

Esses pacotes não são encontrados no repositório padrão do Debian e por isso
foram feitos manualmente.

Agora podemos aplicar o primeiro tema:

```console
$ cd ../../
$ ./i3-themer -a 000
```

{{< figure src="/artigos/i3-para-computadores-antigos/figura3.png" alt="Fonte: O autor (2020)" caption="Figura 3 - Tema aplicado ao desktop">}}

Simples assim, você pode ver todos os temas disponíveis aqui ou se você quiser,
você pode listar todos os temas disponíveis com esse comando:

```console
$ ./i3-themer -l
```

```
000 001 002 003
004 005 006 007
008 009 010 011
012 013 014
```

E aplicar de acordo:

```console
$ ./i3-themer -a 005
```

{{< figure src="/artigos/i3-para-computadores-antigos/figura4.png" alt="Imagem do tema número 5 aplicado ao desktop. Fonte: O autor (2020)" caption="Figura 4 - Tema número 5 aplicado ao desktop">}}

Aqui vai uma lista de teclas de atalho pré-definidas nesses temas:

```
Super+F1		Alsamixer
Super+F2		Navegador web padrão
Super+F3		Gestor de arquivos
Super+F4		Calculadora do gnome
Super+F5		CMUS (aplicativo de música no terminal)
Super+0			Menu de energia do Polybar
Super+D			Menu de aplicativos
Print			Tira screenshot da tela inteira
Shift+Print		Tira screenshot parcial da tela
Alt+Print		Tira screenshot da janela ativa
```

Para demais atalhos, consulte o arquivo de configuração do i3 em
`~/.config/i3/config`.

i3-themer é de código aberto, se puder contribua com a melhoria do programa no
GitHub, seja com novos temas ou código-fonte.

# Aplicativos

Essa parte do artigo é extremamente pessoal, visto que isso depende das
necessidades de cada um. O i3 não faz nada sozinho, então precisamos instalar
alguns aplicativos.

## Navegador web

Um navegador bom e que funciona até em notebooks de 32 bits é o Firefox, para
instalá-lo diretamente em português brasileiro utilize o seguinte comando:

```console
# apt-get install firefox-esr-l10n-pt-br
```

## Editor de texto

Um editor que eu uso para programar é o NeoVim, ele fica ainda mais poderoso
quanto se usa os plugins do site https://vim-bootstrap.com/. Para instalar o
NeoVim use o seguinte comando:

```console
# apt-get install neovim
```

Para instalar o plugins do site que mencionei instale as dependências:

```console
# apt-get install git exuberant-ctags ncurses-term curl
```

Entre no site https://vim-bootstrap.com/, escolha as linguagens de programação
que você usa e baixe o arquivo de configuração. Após o download mova o arquivo
de configuração para a pasta do NeoVim.

```console
$ mkdir -p ~/.config/nvim
$ mv ~/Downloads/generate.vim ~/.config/nvim/init.vim
```

E agora abra o NeoVim para baixar os plugins automaticamente.

```console
$ nvim
```

## Wi-Fi

Como instalei o Debian em um notebook, precisei de uma ferramenta para gerenciar
a rede sem fio. O wicd-curses foi para mim a ferramenta mais simples pois ela
tem uma interface fácil.

```console
# apt-get install wicd-curses
```

Para ver as redes de WIFI disponíveis, digite isso no terminal:

```console
$ wicd-curses
```

{{< figure src="/artigos/i3-para-computadores-antigos/figura5.png" alt="Captura de tela do wicd curses. Fonte: O autor (2020)" caption="Figura 5 - Captura de tela do wicd curses">}}

## Navegador de arquivos

Isso depende inteiramente de você, o gerenciador que escolhi para a máquina
velha é o thunar:

```console
# apt-get install thunar
```

Ele será aberto automaticamente pelo i3 quando você pressionar as teclas
_Super+F3_.

## Git

Se você é um camarada que trabalha com o git, é bom você configurar o seu e-mail
e nome de usuário padrão para não perder seus pontos de contribuição no GitHub.

```console
$ git config --global user.name "seuNomeDeUsuário"
$ git config --global user.email "seu email"
```

É isso, essa é uma forma de você reutilizar um computador antigo se você souber
como se virar com o i3, lógico que mais aplicativos são necessários, tais como,
visualizadores de PDF, de imagens, mas isso depende inteiramente da necessidade
do usuário. O conteúdo deste artigo gerou o seguinte screenshot aqui no VOL:
[Screenshot GNU/Linux &gt; i3 + notebook velho](https://www.vivaolinux.com.br/screenshot/Tiling-window-manager-i3-notebook-velho/)

Over and out.

Fontes:

- Vim bootstrap - https://vim-bootstrap.com/
- i3-themer - https://github.com/cizordj/i3-themer/
