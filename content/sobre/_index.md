---
title: "Sobre"
date: "2023-02-26"
draft: false
description: "A razão pela qual eu criei este site é para organizar os meus pensamentos."
noindex: true
---

# Sobre este site

A razão pela qual eu criei este site é para organizar alguns dos meus
pensamentos. Se tem uma coisa que eu gosto nessa vida é de escrever e isso
venho fazendo desde criança inconscientemente. Hoje como adulto falo duas
línguas e em breve pretendo aprender mais afim de universalizar todos os
artigos que escrevo em todos os idiomas que eu sei.

Neste site eu cago algumas regras sobre como você deve viver a sua vida e
cabe a você decidir se isso presta ou não, não estou aberto a discussões e
nem debates sobre as minhas publicações e tampouco serei responsabilizado
por quaisquer danos materiais advindos de meus tutoriais sobre qualquer
assunto. Eu vou assumir que o leitor saiba o que está fazendo e que vai
prosseguir tudo por sua própria conta e risco. Se algum artigo deste site
te servir para alguma coisa, então eu fico muito satisfeito, caso
contrário apenas ignore.

## Questões frequentes
### Por que os artigos não possuem introdução?
Eu odeio introduções e a maioria das pessoas que costumam ler na internet
costumam pular as introduções também. As introduções servem para encher
linguiça e quase sempre falam coisas que nós já sabemos. Quem se deparar
com os meus textos através de alguma pesquisa no Google já vai dar de cara
com a solução do problema sem precisar rolar uma ou mais páginas de
introduções inúteis.

**Exceção:** A única razão pela qual eu coloco introdução é para contextualizar
algo novo que não é de conhecimento comum. Em outros
casos, se o assunto é de conhecimento comum eu não coloco introdução.

### Por que os artigos não possuem referências?

Já recebi e-mails de pessoas dizendo que sentem falta de algumas
referências nos meus artigos e que sem isso os artigos não possuem valor
acadêmico. Eu até entendo que quem for fazer uma citação precisa mostrar
as fontes, no entanto este site é de cunho pessoal e apresenta as minhas
opiniões e experiências sobre determinados assuntos, se o artigo não
possuir referências é porque representa a minha opinião ou alguma história
de vida. No entanto eu posso fazer algum esforço e apresentar mais dados que
embasam o meu ponto de vista, também vou tentar seguir umas regras de
formatação mais padronizadas como as da ABNT, porém sem prometer nada.
