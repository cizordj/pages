---
title: "Chaves Públicas"
date: "2023-02-26"
draft: false
description: "Quer mandar um e-mail criptografado para mim? Então eis as minhas chaves públicas"
noindex: true
---

# Chaves públicas
Quer enviar um e-mail criptografado pra mim? Então importe as minhas
chaves públicas, se você quiser uma resposta criptografada também não se
esqueça de incluir a sua chave no anexo do e-mail. Se nós ficarmos um bom
tempo sem conversar não esqueça de enviar a sua chave pública novamente
pois eu não costumo manter guardado as chaves de outras pessoas, a não ser
que nós conversemos frequentemente.
```
pub   rsa4096 2021-02-03 [SC] [expired: 2022-02-03]
      B8BE751A2C525B45668BDA8B713A8B49582E5834
uid   [ expired] Cézar ****** <cezar******@protonmail.com>
```
Chave indisponível

```
pub   rsa3072 2020-12-08 [SC]
      16DC13CE15C3BA0053383E689466E26E3D20204C
uid           [ultimate] Cézar ****** <32869222+cizordj@users.noreply.github.com>
sub   rsa3072 2020-12-08 [E]
```
[Chave atual](/gpg/public_key.asc)
