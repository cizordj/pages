---
description: "Bem-vindo ao meu site pessoal, aqui você encontra todos os meus artigos e alguns dos meus programas, tenha uma ótima visita."
framed: false
noindex: false
---
{{< profile >}}

## Projetos

Essa é uma lista não exaustiva de projetos pessoais a qual trabalhei e ainda trabalho.

| Nome | Descrição |
| -------- | -------- |
| Molotov | Cria pendrives de Windows no Linux |
| molotov-debian | Pacote Debian do molotov |
| i3-themer | Temas para o i3wm |
| colorblocks | Barra de informações para o sistema |
| websites | Sites aleatórios que produzi |
| yancfs | Programa em lua que configura minha IDE |
| rafinha-api | Banco de imagens feito em PHP |
| stterm | Terminal Linux customizado |

## Linguagens

As quais gosto de trabalhar e que possuo experiência.

| Nome | Contexto |
| --------- | -------- |
| Shellscript | Sistema operacional |
| PHP | web |
| Typescript | web |
| Lua | Sistema operacional |

Linguagens que eu sei, mas detesto.

| Linguagem | Contexto |
| --------- | -------- |
| C | Sistema operacional |
| Javascript | web |
| TL (Typed Lua) | Sistema operacional |
| HTML | web |
| CSS | web |

É isso, você pode dar um passeio, ler os artigos, entrar em contato, ver os projetos, fique à vontade 😉

---
